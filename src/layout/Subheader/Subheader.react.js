import React, { Component } from "react";
//import DateRangePicker from "react-bootstrap-daterangepicker";
//import { Button, Dropdown } from "react-bootstrap";

import "bootstrap-daterangepicker/daterangepicker.css";
import "./Subheader.css";

class Subheader extends Component {
    constructor(props) {
        super(props);      
    }
  render() {
   
    return (
      <div className="row submenu">
        <div className="container-fluid " id="submenu-container">
          <div className="row">
            {/* Submenu section starts */}
            <nav className="navbar col-12 ">
              <div className="dropdown mr-auto d-flex d-sm-none">
                <button className="btn dropdown-toggle btn-sm btn-primary" type="button" data-toggle="dropdown" aria-haspopup="true" aria-expanded="false" >Dashboard</button>
                <div className="dropdown-menu">
                  <a className="dropdown-item" href="#" onClick={e => e.preventDefault()} >
                    Dashboard
                  </a>
                  <a className="dropdown-item" href="#" onClick={e => e.preventDefault()}>
                    Featured
                  </a>
                  <a className="dropdown-item" href="#" onClick={e => e.preventDefault()} >
                    popular
                  </a>
                </div>
              </div>
              <ul className="nav nav-pills mr-auto d-none d-sm-flex">
                <li className="nav-item "> 
                    <a className="nav-link active" href="#" onClick={e => e.preventDefault()}>
                     Dashboard
                    </a>
                </li>
                <li className="nav-item">
                  <a className="nav-link" href="#" onClick={e => e.preventDefault()}>
                    Featured
                  </a>
                </li>
                <li className="nav-item">
                  <a className="nav-link" href="#" onClick={e => e.preventDefault()} >
                    popular
                  </a>
                </li>
              </ul>
              <ul className="nav nav-pills ml-auto d-none d-xl-flex">
                <li className="nav-item">
                  <a className="nav-link" href="#" onClick={e => e.preventDefault()}>
                    Today
                  </a>
                </li>
                <li className="nav-item">
                  <a className="nav-link" href="#" onClick={e => e.preventDefault()}>
                    This week
                  </a>
                </li>
                <li className="nav-item">
                  <a className="nav-link" href="#" onClick={e => e.preventDefault()}>
                    This Month
                  </a>
                </li>
              </ul>
            </nav>
            {/* Submenu section ends */}
          </div>
        </div>
      </div>
    );
  }
}

export default Subheader;
