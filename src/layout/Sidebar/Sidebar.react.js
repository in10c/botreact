import React, { useState, useEffect } from "react";
import {
  BrowserRouter as Router,
  NavLink
} from "react-router-dom";
import { useLocation} from "react-router";

export default function Sidebar(props) {
  let location = useLocation();
  const [menuElegido, ponerMenu] = useState('dashboard');
  useEffect(()=>{
    ponerMenu(location.pathname)
  }, [location.pathname])
  return (
    <div className="sidebar">
      <a href='#' onClick={e => e.preventDefault()} className="logo">
        
        <div className="logo-text">
          <h5 className="fs22 mb-0">
            Bot Arbitraje
          </h5>
          <p className="text-uppercase fs11">Escrypser</p>
        </div>
      </a>
      <h6 className="subtitle fs11">Menú principal</h6>
      <ul className="nav flex-column">
        <div className={`nav-item ${menuElegido ==='/' ? 'active' : ''}`}>
          <NavLink className="nav-link" to="/" activeClassName="active">
            <i className="material-icons icon">report_problem</i>
            <span>Alertas</span>
          </NavLink>
        </div>  
        <div className={`nav-item ${menuElegido ==='/explorar' ? 'active' : ''}`}>
          <NavLink className="nav-link" to="/explorar" activeClassName="active">
            <i className="material-icons icon">explore</i>
            <span>Explorar</span>
          </NavLink>
        </div>  
        <div className={`nav-item ${menuElegido ==='/configuracion' ? 'active' : ''}`}>
          <NavLink className="nav-link" to="/configuracion" activeClassName="active">
            <i className="material-icons icon">build</i>
            <span>Configuraciones</span>
          </NavLink>
        </div>  
      </ul>
    </div>
  );
  
}
