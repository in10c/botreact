import React from 'react';



const Footer = () => {

    return (
        <footer className="footer" style={{ marginTop: "-58px" }}>
            <div className="container-fluid">
                <div className="row">
                    <div className="col-12 col-md text-center text-md-left align-self-center">
                        <p>Powered by <a href='https://escrypser.com' target="_blank">Escrypser</a></p>
                    </div>
                </div>
            </div>
        </footer>
    );

}

export default Footer;