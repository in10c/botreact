import React, { Component } from "react";
import { Button, Form } from "react-bootstrap";
import Profiledropdown from "./components/profiledropdown/profiledropdown.react";
import Messages from "./components/Messages/Messages.react";
import Flags from "./components/Flags/Flags.react";
import Cart from "./components/Cart/Cart.react";
import Tasks from "./components/Tasks/Tasks.react";
import Quicklinks from "./components/Quicklinks/Quicklinks.react";

class Header extends Component {
  toggleSidebar() {
    document.body.classList.toggle("sidemenu-open");
  }
  render() {
    return (
      <div className="row header">
        <div className="container-fluid " id="header-container">
          <div className="row">
            {/* Header starts */}
            <nav className="navbar col-12 navbar-expand ">
              <button
                className="menu-btn btn btn-link btn-sm"
                type="button"
                onClick={this.toggleSidebar}
              >
                <i className="material-icons">menu</i>
              </button>
              <div
                className="collapse navbar-collapse"
                id="navbarSupportedContent"
              >
                {/* search starts */}
                <form className="form-inline search mr-auto">
                  <Form.Control type="search" placeholder="Search" size="sm" />
                  <Button variant="link" size="sm" type="submit">
                    <i className="material-icons">search</i>
                  </Button>
                </form>
                {/* search ends */}
                {/* large desktop market rates starts */}
                <div className="mx-auto d-none d-xxl-inline">
                  <div className="row mx-0">
                    <div className="col-auto pr-0 align-self-center">
                      <i className="material-icons vm">public</i>
                    </div>
                    <div className="col-auto">
                      <h5 className="fs15 font-weight-normal mb-0">
                        Market{" "}
                        <span className="text-danger mx-1">
                          <i className="material-icons vm">arrow_drop_down</i>
                        </span>{" "}
                        254608 $
                      </h5>
                      <p className="fs11">
                        <span>Live</span>{" "}
                        <span className="text-danger">-0.1%</span>{" "}
                        <span>(2487 $)</span>
                      </p>
                    </div>
                    <div className="col-auto border-left-dashed">
                      <h5 className="fs15 font-weight-normal mb-0">
                        Update{" "}
                        <span className="text-success mx-1">
                          <i className="material-icons vm">arrow_drop_up</i>
                        </span>{" "}
                        254608 $
                      </h5>
                      <p className="fs11">
                        <span>Live</span>{" "}
                        <span className="text-success">+0.1%</span>{" "}
                        <span>(2487 $)</span>
                      </p>
                    </div>
                  </div>
                </div>
                {/* large desktop market rates ends */}
                {/* icons dropwdowns starts */}
                <div className="navbar-nav ml-auto">
                  {/* flag dropdown*/}
                  <Flags />
                  {/* applicatio quicklinks dropdown*/}
                  <Quicklinks />
                  {/* cart dropdown*/}
                  <Cart />
                  {/* task dropdown*/}
                  <Tasks />
                  {/* message dropdown*/}
                  <Messages />
                  {/* profile dropdown*/}
                  <Profiledropdown />
                </div>
                {/* icons dropwdowns starts */}
              </div>
            </nav>
            {/* Header ends */}
          </div>
        </div>
      </div>
    );
  }
}

export default Header;
