import React from "react";
import {
    Route,
    BrowserRouter as Router,
    NavLink,
    Switch
} from "react-router-dom";

import logoimg from '../assets/img/logoicon.svg';
import teamimg from '../assets/img/team.jpg';

var sectionStyle = {
    backgroundImage: 'url(' + teamimg + ')'
};

class Login extends React.Component {
    constructor(props) {
        super(props);
        this.state = { isLoggedIn: true, correo : "", contrasena: "" };

        // This binding is necessary to make `this` work in the callback
        this.handleClick = this.handleClick.bind(this);
    }

    handleClick(e) {
        e.preventDefault()
        if(this.state.correo=="administracion@escrypser.com" && this.state.contrasena == "abc12345"){
            this.setState({
                isLoggedIn: true
            });    
            this.props.onLogin(this.state.isLoggedIn); 
        } else {
            alert("El usuario y/o la contraseña no son correctos.")
        }
           
    }

    render() {
        return (
            <div className="wrapper">
                <div className="content shadow-sm position-relative">
                    <header className="header">
                        {/* Fixed navbar */}
                        <nav className="container-fluid">
                            <div className="row">
                                <div className="col align-self-center">
                                    <a className="logo text-white">
                                        <img src={logoimg} alt="adminux pro" className="logo-icon" />
                                        <div className="logo-text">
                                            <h5 className="fs22 mb-0">
                                                AdminUX <sup className="badge badge-danger">PRO</sup>
                                            </h5>
                                            <p className="text-uppercase fs11">Admin Dashboard</p>
                                        </div>
                                    </a>
                                </div>
                            </div>
                        </nav>
                    </header>
                    <div className="background opac blur" style={sectionStyle}>
                    </div>
                    {/* Main container starts */}
                    <div className="container main-container" id="main-container">
                        <div className="row login-row-height" style={{ minHeight: "calc(100vh - 300px)" }}>
                            <div className="col-12 col-md-6 col-lg-5 col-xl-4 mx-auto align-self-center">
                                <div className="card border-0 shadow-lg blur">
                                    <div className="card-body py-5">
                                        <h5 className="font-weight-light mb-1 text-mute-high">
                                            Bienvenido, </h5>
                                        <h3 className="font-weight-normal mb-4">Por favor accede</h3>
                                        <div className="card  mb-2 overflow-hidden">
                                            <div className="card-body p-0">
                                                <label htmlFor="inputEmail" className="sr-only">Correo electrónico </label>
                                                <input type="email" id="inputEmail" className="form-control rounded-0 border-0" value={this.state.correo} onChange={ev=>{
                                                    ev.persist()
                                                    this.setState({correo: ev.target.value})
                                                }} placeholder="Email address" required autoFocus />
                                                <hr className="my-0" />
                                                <label htmlFor="inputPassword" className="sr-only">
                                                    Contraseña </label>
                                                <input type="password" id="inputPassword" className="form-control rounded-0 border-0" value={this.state.contrasena} onChange={ev=>{
                                                    ev.persist()
                                                    this.setState({contrasena: ev.target.value})
                                                }} placeholder="Password" required />
                                            </div>
                                        </div>
                                        <div className="mb-4">                                             
                                            <button onClick={this.handleClick} className=" btn btn-primary btn-block" >Acceder <i className="material-icons md-18">arrow_forward</i> </button>
                                        </div>
                                    </div>
                                </div>
                            </div>
                        </div>
                    </div>
                    {/* Main container ends */}
                </div>
                {/* wrapper ends */}
            </div>
        );
    }
}

export default Login;
