const Variables = {
    productivo: false,
    regresaURLPrincipal: ()=>{
        if(Variables.productivo){
            return 'https://panel.escrypser.com/API'
        } else {
            return 'http://escrypser.com:3001'
        }
    }
};

export default Variables