import * as axios from 'axios';
import Variables from './Variables';

const Peticiones = {
    post : (url, payload) =>{
        return axios.post(Variables.regresaURLPrincipal()+"/"+url, payload)
    },
    get: (url) =>{
        return axios.get(Variables.regresaURLPrincipal()+"/"+url)
    }
};
export default Peticiones