import React from "react";
import { BrowserRouter as Router, Switch, Route } from "react-router-dom";
import { ToastProvider } from 'react-toast-notifications'
import Header from "../layout/header/header.react";
import Subheader from "../layout/Subheader/Subheader.react";
import Sidebar from "../layout/Sidebar/Sidebar.react";
import Footer from "../layout/Footer/Footer.react";

import Production from "./Production/Production.react";
import Networking from "./Networking/Networking.react";
import Servererror from "./Servererror/Servererror.react";
import Notfound from "./Notfound/Notfound.react";
import ConfiguracionGeneral from "./Configuraciones/configuracionGeneral"
import ConfiguracionAlias from "./Configuraciones/configuracionAias"
import configuracionGrupos from "./Configuraciones/configuracionGrupos"
import ConfiguracionPares from "./Configuraciones/configuracionPares"
import Comparativa from "./Panel/Comparativa"
import Alertas from "./Panel/Alertas"
import PreviaTransaccion from "./Panel/PreviaTransaccion"
import ConfiguracionAPIs from "./Configuraciones/configuracionAPIs"
import configuracionRutas from "./Configuraciones/configuracionRutas"
import "./dashboard.css";

class Dashboard extends React.Component {
  constructor(props) {
    super(props);
  }

  render() {
    const dom = document.querySelector("body");
    if (window.innerWidth <= 992) {
      dom.classList.remove("sidemenu-open");
    } else {
      dom.classList.add("sidemenu-open");
    }


    return (
      <div>
        {/* Sidebar ends */}
        <Sidebar />
        {/* Sidebar ends */}
        
        {/* wrapper starts */}
        <div className="wrapper">
          <div className="content shadow-sm"  style={{ paddingBottom: "58px" }}>
            <div className="container-fluid header-container">
              {/* header starts */}
              <Header />
              {/* header ends */}

              {/* subheader starts */}
              
              {/* subheader starts */}
            </div>

            {/* A <Switch> looks through its children <Route>s and
            renders the first one that matches the current URL. */}
            <ToastProvider>
              <Switch>
                <Route exact path="/" component={Alertas} />
                <Route exact path="/explorar" component={Comparativa} />
                <Route path="/networking" component={Networking} />
                <Route path="/production" component={Production} />
                <Route path="/servererror" component={Servererror} />
                <Route path="/notfound" component={Notfound} />
                <Route path="/configuracion" component={ConfiguracionGeneral} />
                <Route path="/alias" component={ConfiguracionAlias} />
                <Route path="/grupos" component={configuracionGrupos} />
                <Route path="/rutas" component={configuracionRutas} />
                <Route path="/pares" component={ConfiguracionPares} />
                <Route path="/apis" component={ConfiguracionAPIs} />
                <Route path="/transaccion/:emisorExchange/:receptorExchange/:alias" component={PreviaTransaccion} />
              </Switch>
            </ToastProvider>
          </div>
          {/* footer */}
          <Footer />
          {/* footer ends */}
        </div>
        {/* wrapper starts */}
      </div>
    );
  }
}

export default Dashboard;
