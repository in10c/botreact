import React, {useEffect, useState} from "react";
import { useToasts } from 'react-toast-notifications'
import Peticiones from '../../servicios/Peticiones'
import CabezalConfig from "./cabezalConfig"
import MenuConfiguracion from "./menuConfiguracion"


export default function ConfiguracionAPIs(){
    const [formulario, ponerFormulario] = useState({});
    const [listaAPIs, ponerAPIS] = useState([]);
    const [listaExchanges, ponerExchanges] = useState([]);
    const { addToast } = useToasts()
    const handleInputChange = (event) => {
        event.persist();
        ponerFormulario(formulario => ({...formulario, [event.target.name]: event.target.value}));
    }   
    const traerAPIS = () => {
        Peticiones.get("obtenerAPIs").then(resp=>{
            console.log("traigo resp de apis", resp)
            if(resp.data.success==1){
                ponerAPIS(resp.data.apis)
            }
            //ponerAlias(resp.data.alias)
        }, err=>{
            console.log("ocurrio error", err)
        })
    }
    const handleSubmit = (ev) => {
        ev.preventDefault();
        Peticiones.post("registrarAPI", formulario).then(resp=>{
            ponerFormulario({
                exchangeID: formulario.exchangeID,
                apiKey: "",
                apiSecret: ""
            })
            addToast("Los cambios han sido guardados correctamente.", { appearance: 'success', autoDismiss: true, pauseOnHover: true })
            traerAPIS()
        }, err=>{
            console.log("ocurrio error", err)
        })
    }
    const editarPar = (parElegido) => {
        ponerFormulario({
            exchangeID: parElegido.clavesapi_exchanges_id,
            apiID: parElegido.clavesapi_id,
            apiKey: parElegido.clavesapi_key,
            apiSecret: parElegido.clavesapi_key
        });
    }
    const eliminarConfig = (clavesapi_id) => {
        Peticiones.post("eliminarConfiguracion", {
            apisID: clavesapi_id
        }).then(resp=>{
            addToast("La configuración ha sido eliminada correctamente", { appearance: 'success', autoDismiss: true, pauseOnHover: true })
            traerAPIS()
        }, err=>{
            console.log("ocurrio error", err)
        })
    }
    const copiarValor = (valor, tipo) =>{
        navigator.clipboard.writeText(valor).then(function() {
            addToast(tipo+" copiada correctamente.", { appearance: 'success', autoDismiss: true, pauseOnHover: true })
        });
    }
    useEffect(() => {
        console.log("inicia use effect")
        Peticiones.get("exchangesDisponibles").then(resp=>{
            console.log("traigo resp exchanges", resp)
            ponerExchanges(resp.data.exchanges)
            ponerFormulario(formulario => ({...formulario, ["exchangeID"]: resp.data.exchanges[0].exchanges_id}));
            traerAPIS()
        }, err=>{
            console.log("ocurrio error", err)
        })
    }, [])

    return(
            <div className="container-fluid main-container pt-0" id="main-container">
                <CabezalConfig/>
                <div className="row">
                    <div className="container">
                        <div className="row">
                            <div className="col-12 col-md-4 col-lg-3">
                                <MenuConfiguracion />
                            </div>
                            <div className="col">   
                                <form className="card mb-4 shadow-sm border-0" onSubmit={handleSubmit}>
                                    <div className="card-body">
                                        <div className="row">
                                            <div className="col-12 col-md-12 col-lg-4">
                                                <div className="form-group">
                                                    <label>Exchange</label>
                                                    <select className="form-control" name="exchangeID" onChange={handleInputChange} value={formulario.exchangeID}>
                                                        {listaExchanges.map(exchange=>(
                                                            <option value={exchange.exchanges_id}>{exchange.exchanges_nombre}</option>
                                                        ))}
                                                    </select>
                                                </div>
                                            </div>
                                            <div className="col-12 col-md-12 col-lg-4">
                                                <div className="form-group">
                                                    <label>API Key</label>
                                                    <input type="text" className="form-control" name="apiKey" onChange={handleInputChange} value={formulario.apiKey}/>
                                                </div>
                                            </div>
                                            <div className="col-12 col-md-12 col-lg-4">
                                                <div className="form-group">
                                                    <label>API Secret</label>
                                                    <input type="text" className="form-control" name="apiSecret" onChange={handleInputChange} value={formulario.apiSecret}/>
                                                </div>
                                            </div>
                                        </div>
                                    </div>
                                    <div className="card-footer">
                                        <button className="btn btn-primary float-right">{formulario.apiID ? "Guardar cambios" : "Agregar nuevo par"}</button>
                                    </div>
                                </form>
                                <div className="card border-0 shadow-sm mb-4">
                                    <div className="card-header border-0 bg-none">
                                        <div className="row">
                                            <div className="col-12 col-md">
                                                <p class="fs15">Lista de pares a comparar</p>
                                            </div>
                                        </div>
                                    </div>
                                    <div className="card-body">
                                        <table className="table w-100 mb-0">
                                            <thead>
                                                <tr>
                                                    <th className="all">ID</th>
                                                    <th className="all">Exchange</th>
                                                    <th className="all">API Key</th>
                                                    <th className="all">API Secret</th>
                                                    <th className="all">Acciones</th>
                                                </tr>
                                            </thead>
                                            <tbody>
                                                {
                                                    // clavesapi_id: 6
                                                    // clavesapi_exchanges_id: 2
                                                    // clavesapi_key: "asd65as6dad57a6"
                                                    // clavesapi_secret: "ddsd222das78d6asd7"
                                                    listaAPIs.map((par, indice)=>(
                                                         <tr>
                                                            <td>{par.clavesapi_id}</td>
                                                            <td>{par.exchanges_nombre}</td>
                                                            <td>
                                                                {par.clavesapi_key ? par.clavesapi_key.substring(0, 10)+"..." : ""} 
                                                                <button type="button" class="mb-2 shadow-sm mr-2 btn btn-link text-primary" onClick={ev=>copiarValor(par.clavesapi_key, "API Key")}>
                                                                    <i class="material-icons">content_copy</i>
                                                                </button>
                                                            </td>
                                                            <td>
                                                                {par.clavesapi_secret ? par.clavesapi_secret.substring(0, 10)+"..." : ""} 
                                                                <button type="button" class="mb-2 shadow-sm mr-2 btn btn-link text-primary" onClick={ev=>copiarValor(par.clavesapi_secret, "API Secret")}>
                                                                    <i class="material-icons">content_copy</i>
                                                                </button></td>
                                                            <td>
                                                                <button type="button" class="mb-2 shadow-sm mr-2 btn btn-link text-primary" onClick={ev=>{
                                                                    ev.preventDefault()
                                                                    editarPar(par)
                                                                }}><i class="material-icons">edit</i></button>
                                                                <button type="button" class="mb-2 shadow-sm mr-2 btn btn-link text-primary" onClick={ev=>{
                                                                    ev.preventDefault()
                                                                    if(window.confirm("¿Está seguro que desea eliminar la configuración para "+par.exchanges_nombre+"?")){
                                                                        eliminarConfig(par.clavesapi_id)
                                                                    }
                                                                }}><i class="material-icons">delete</i></button>
                                                            </td>
                                                        </tr>
                                                    ))
                                                }
                                                
                                            </tbody>
                                        </table>
                                    </div>
                                </div>
                            </div>
                        </div>
                    </div>
                </div>

            </div>
    )
} 