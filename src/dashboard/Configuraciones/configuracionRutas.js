import React, {useEffect, useState} from "react";
import { useToasts } from 'react-toast-notifications'
import Peticiones from '../../servicios/Peticiones'
import CabezalConfig from "./cabezalConfig"
import MenuConfiguracion from "./menuConfiguracion"


export default function ConfiguracionRutas(){
    const [formulario, ponerFormulario] = useState({aliasElegido: null});
    const [listaAlias, ponerAlias] = useState([]);
    const [aliasElegido, escogerAlias] = useState(null);
    const [listaPares, ponerPares] = useState([]);
    const [listaExchanges, ponerExchanges] = useState([]);
    const { addToast } = useToasts()
    const handleInputChange = (event) => {
        event.persist();
        ponerFormulario(formulario => ({...formulario, [event.target.name]: event.target.value}));
    }
    const actualizarAlias = () =>{
        Peticiones.post("obtenerFilas", {coleccion: "grupos"}).then(resp=>{
            console.log("traigo resp alias", resp)
            if(resp.data.success==1){
                ponerAlias(resp.data.datos)
                escogerAlias(resp.data.datos[0].grupos_id)
            }
        }, err=>{
            console.log("ocurrio error", err)
        })
    }
    const actualizarPares = (aliasID) => {
        Peticiones.post("obtenerRutas", { grupoID: aliasID }).then(resp=>{
            console.log("traigo resp de pares", resp)
            if(resp.data.success==1){
                ponerPares(resp.data.rutas)
            }
            //ponerAlias(resp.data.alias)
        }, err=>{
            console.log("ocurrio error", err)
        })
    }
    const handleSubmit = (ev) => {
        ev.preventDefault();
        let auxiliar = JSON.parse(JSON.stringify(formulario))
        auxiliar.grupoID = aliasElegido
        Peticiones.post("registrarRuta", auxiliar).then(resp=>{
            ponerFormulario({
                tipo: formulario.tipo,
                parPrimario: "",
                parSecundario: ""
            })
            addToast("Los cambios han sido guardados correctamente.", { appearance: 'success', autoDismiss: true, pauseOnHover: true })
            actualizarPares(aliasElegido)
        }, err=>{
            console.log("ocurrio error", err)
        })
    }
    const eliminarPar = (parID) =>{
        Peticiones.post("eliminarRuta", {rutaID: parID}).then(resp=>{
            ponerFormulario({
                tipo: formulario.tipo,
                parPrimario: "",
                parSecundario: ""
            })
            addToast("El par ha sido eliminado correctamente", { appearance: 'success', autoDismiss: true, pauseOnHover: true })
            actualizarPares(aliasElegido)
        }, err=>{
            console.log("ocurrio error", err)
        })
    }
    const editarPar = (parElegido) => {
        ponerFormulario({
            tipo: parElegido.rutas_tipo,
            rutaID: parElegido.rutas_id,
            parPrimario: parElegido.rutas_parentrada,
            parSecundario: parElegido.rutas_parsalida
        });
    }
    useEffect(() => {
        actualizarAlias()
    }, [])

    useEffect(()=>{
        actualizarPares(aliasElegido)
    }, [aliasElegido])

    return(
            <div className="container-fluid main-container pt-0" id="main-container">
                <CabezalConfig/>
                <div className="row">
                    <div className="container">
                        <div className="row">
                            <div className="col-12 col-md-4 col-lg-3">
                                <MenuConfiguracion />
                            </div>
                            <div className="col">
                                <div className="card mb-4 shadow-sm border-0" onSubmit={handleSubmit}>
                                    <div className="card-body">
                                        <div className="row">
                                            <div className="col-12 col-md-12 col-lg-12">
                                                <div className="form-group">
                                                    <label>Grupo de rutas</label>
                                                    <select className="form-control" name="aliasElegido" onChange={(event) => {
                                                            event.persist();
                                                            console.log("se pone nuevo alias", event.target.value)
                                                            escogerAlias( event.target.value);
                                                        }} value={aliasElegido}>
                                                        {listaAlias.map(alias=>(
                                                            <option value={alias.grupos_id}>{alias.grupos_alias}</option>
                                                        ))}
                                                    </select>
                                                </div>
                                            </div>
                                        </div>
                                    </div>
                                </div>
                                <form className="card mb-4 shadow-sm border-0" onSubmit={handleSubmit}>
                                    <div className="card-body">
                                        <div className="row">
                                            
                                            <div className="col-12 col-md-12 col-lg-4">
                                                <div className="form-group">
                                                    <label>Par de entrada</label>
                                                    <input type="text" className="form-control" placeholder="Por ejemplo BITCOIN" name="parPrimario" onChange={handleInputChange} value={formulario.parPrimario}/>
                                                </div>
                                            </div>
                                            <div className="col-12 col-md-12 col-lg-4">
                                                <div className="form-group">
                                                    <label>Par de salida</label>
                                                    <input type="text" className="form-control" placeholder="Por ejemplo BITCOIN" name="parSecundario" onChange={handleInputChange} value={formulario.parSecundario}/>
                                                </div>
                                            </div>
                                            <div className="col-12 col-md-12 col-lg-4">
                                                <div className="form-group">
                                                    <label>Tipo</label>
                                                    <select className="form-control" name="tipo" onChange={handleInputChange} value={formulario.tipo}>
                                                        <option value={null} disabled selected>Seleccione uno...</option>
                                                        <option value="comprar">Comprar</option>
                                                        <option value="vender">Vender</option>
                                                    </select>
                                                    
                                                </div>
                                            </div>
                                        </div>
                                    </div>
                                    <div className="card-footer">
                                        <button className="btn btn-primary float-right">{formulario.parID ? "Guardar cambios en el par" : "Agregar nuevo par"}</button>
                                    </div>
                                </form>
                                <div className="card border-0 shadow-sm mb-4">
                                    <div className="card-header border-0 bg-none">
                                        <div className="row">
                                            <div className="col-12 col-md">
                                                <p class="fs15">Lista de pares a comparar</p>
                                            </div>
                                        </div>
                                    </div>
                                    <div className="card-body">
                                        <table className="table w-100 mb-0">
                                            <thead>
                                                <tr>
                                                    <th className="all">ID</th>
                                                    <th className="all">Par de entrada</th>
                                                    <th className="all">Par de salida</th>
                                                    <th className="all">Tipo</th>
                                                    <th className="all">Acciones</th>
                                                </tr>
                                            </thead>
                                            <tbody>
                                                {
                                                    listaPares.map((par, indice)=>(
                                                         <tr>
                                                            <td>{par.rutas_id}</td>
                                                            <td>{par.rutas_parentrada}</td>
                                                            <td>{par.rutas_parsalida}</td>
                                                            <td>{par.rutas_tipo}</td>
                                                            <td>
                                                                <button type="button" class="mb-2 shadow-sm mr-2 btn btn-link text-primary" onClick={ev=>{
                                                                    ev.preventDefault()
                                                                    editarPar(par)
                                                                }}><i class="material-icons">edit</i></button>
                                                                <button type="button" class="mb-2 shadow-sm mr-2 btn btn-link text-primary" onClick={ev=>{
                                                                    ev.preventDefault()
                                                                    if(window.confirm("¿Está seguro que desea eliminar el par "+par.rutas_parentrada+"/"+par.rutas_parsalida+"?")){
                                                                        eliminarPar(par.rutas_id)
                                                                    }
                                                                }}><i class="material-icons">delete</i></button>
                                                            </td>
                                                        </tr>
                                                    ))
                                                }
                                                
                                            </tbody>
                                        </table>
                                    </div>
                                </div>
                            </div>
                        </div>
                    </div>
                </div>

            </div>
    )
} 