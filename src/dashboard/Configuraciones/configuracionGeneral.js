import React, {useEffect, useState} from "react";
import { useToasts } from 'react-toast-notifications'
import Peticiones from '../../servicios/Peticiones'
import MenuConfiguracion from "./menuConfiguracion"
import CabezalConfig from "./cabezalConfig"

export default function ConfiguracionGeneral(){
    const [formulario, ponerFormulario] = useState({});
    const { addToast } = useToasts()
    const handleInputChange = (event) => {
        event.persist();
        ponerFormulario(formulario => ({...formulario, [event.target.name]: event.target.value}));
    }
    useEffect(() => {
        console.log("inicia use effect")
        Peticiones.get("obtenerConfiguracion").then(resp=>{
            console.log("traigo respuesta", resp)
            ponerFormulario({
                porcentajeMinimo: resp.data.configuracion.configuracion_porcentajeminimo,
                montoOperacion: resp.data.configuracion.configuracion_montooperacion
            })
        }, err=>{
            console.log("ocurrio error", err)
        })
    }, [])
    const handleSubmit = (ev) => {
        ev.preventDefault();
        Peticiones.post("guardarConfiguracion", formulario).then(resp=>{
            addToast("Los cambios han sido guardados correctamente.", { appearance: 'success', autoDismiss: false, pauseOnHover: true })
        }, err=>{
            console.log("ocurrio error", err)
        })
    }
    return(
            <div className="container-fluid main-container pt-0" id="main-container">
                <CabezalConfig/>
                <div className="row">
                    <div className="container">
                        <div className="row">
                            <div className="col-12 col-md-4 col-lg-3">
                                <MenuConfiguracion />
                            </div>
                            <div className="col">
                                <form className="card mb-4 shadow-sm border-0" onSubmit={handleSubmit}>
                                    <div className="card-body">
                                        <div className="row">
                                            <div className="col-12 col-md-12 col-lg-12">
                                                <div className="form-group">
                                                    <label>Porcentaje mínimo de ganancia</label>
                                                    <input type="text" className="form-control" placeholder=".60%" name="porcentajeMinimo" onChange={handleInputChange} value={formulario.porcentajeMinimo}/>
                                                </div>
                                            </div>
                                            <div className="col-12 col-md-12 col-lg-12">
                                                <div className="form-group">
                                                    <label>Monto por operación</label>
                                                    <input type="number" className="form-control" placeholder="1000 USD" name="montoOperacion" onChange={handleInputChange} value={formulario.montoOperacion}/>
                                                </div>
                                            </div>
                                        </div>
                                    </div>
                                    <div className="card-footer">
                                        <button className="btn btn-primary float-right">Guardar cambios</button>
                                    </div>
                                </form>
                            </div>
                        </div>
                    </div>
                </div>

            </div>
    )
} 