import React from "react"
import {
    NavLink
} from "react-router-dom";

export default function MenuConfiguracion(){

    return (
        <div className="card mb-4 shadow-sm border-0">
            <div className="card-body text-left">
                <div className="page-subtitle">Menú de configuración</div>
                <ul className="nav nav-pills nav-fill flex-column sidemenu">
                    <li className="nav-item">
                        <NavLink className="nav-link text-left" activeClassName="active" to="/configuracion">Configuracion general</NavLink>
                    </li>
                    <li className="nav-item">
                        <NavLink className="nav-link text-left" activeClassName="active" to="/alias">Alias de pares</NavLink>
                    </li>                                           
                    <li className="nav-item">
                        <NavLink className="nav-link text-left" activeClassName="active" to="/pares">Pares por exchanges</NavLink>
                    </li>
                    <li className="nav-item">
                        <NavLink className="nav-link text-left" activeClassName="active" to="/grupos">Alias de grupos</NavLink>
                    </li>                                           
                    <li className="nav-item">
                        <NavLink className="nav-link text-left" activeClassName="active" to="/rutas">Rutas de pares</NavLink>
                    </li>
                    <li className="nav-item">
                        <NavLink className="nav-link text-left" activeClassName="active" to="/apis">APIs registradas</NavLink>
                    </li>
                </ul>
            </div>
        </div>
    )
}