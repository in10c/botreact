import React, {useEffect, useState} from "react";
import { useToasts } from 'react-toast-notifications'
import Peticiones from '../../servicios/Peticiones'
import CabezalConfig from "./cabezalConfig"
import MenuConfiguracion from "./menuConfiguracion"


export default function ConfiguracionPares(){
    const [formulario, ponerFormulario] = useState({aliasElegido: null});
    const [listaAlias, ponerAlias] = useState([]);
    const [aliasElegido, escogerAlias] = useState(null);
    const [listaPares, ponerPares] = useState([]);
    const [listaExchanges, ponerExchanges] = useState([]);
    const { addToast } = useToasts()
    const handleInputChange = (event) => {
        event.persist();
        ponerFormulario(formulario => ({...formulario, [event.target.name]: event.target.value}));
    }
    const actualizarAlias = () =>{
        Peticiones.get("obtenerAlias").then(resp=>{
            console.log("traigo resp alias", resp)
            if(resp.data.success==1){
                ponerAlias(resp.data.alias)
                escogerAlias(resp.data.alias[0].aliaspares_id)
            }
        }, err=>{
            console.log("ocurrio error", err)
        })
    }
    const actualizarPares = (aliasID) => {
        Peticiones.post("obtenerPares", { aliasID }).then(resp=>{
            console.log("traigo resp de pares", resp)
            if(resp.data.success==1){
                ponerPares(resp.data.pares)
            }
            //ponerAlias(resp.data.alias)
        }, err=>{
            console.log("ocurrio error", err)
        })
    }
    const handleSubmit = (ev) => {
        ev.preventDefault();
        let auxiliar = JSON.parse(JSON.stringify(formulario))
        auxiliar.aliasID = aliasElegido
        Peticiones.post("registrarPar", auxiliar).then(resp=>{
            ponerFormulario({
                exchangeID: formulario.exchangeID,
                parPrimario: "",
                parSecundario: ""
            })
            addToast("Los cambios han sido guardados correctamente.", { appearance: 'success', autoDismiss: true, pauseOnHover: true })
            actualizarPares(aliasElegido)
        }, err=>{
            console.log("ocurrio error", err)
        })
    }
    const eliminarPar = (parID) =>{
        Peticiones.post("eliminarPar", {parID}).then(resp=>{
            ponerFormulario({
                exchangeID: formulario.exchangeID,
                parPrimario: "",
                parSecundario: ""
            })
            addToast("El par ha sido eliminado correctamente", { appearance: 'success', autoDismiss: true, pauseOnHover: true })
            actualizarPares(aliasElegido)
        }, err=>{
            console.log("ocurrio error", err)
        })
    }
    const editarPar = (parElegido) => {
        ponerFormulario({
            exchangeID: parElegido.pares_exchanges_id,
            parID: parElegido.pares_id,
            parPrimario: parElegido.pares_primario,
            parSecundario: parElegido.pares_secundario
        });
    }
    useEffect(() => {
        console.log("inicia use effect")
        Peticiones.get("exchangesDisponibles").then(resp=>{
            console.log("traigo resp exchanges", resp)
            ponerExchanges(resp.data.exchanges)
            ponerFormulario(formulario => ({...formulario, ["exchangeID"]: resp.data.exchanges[0].exchanges_id}));
        }, err=>{
            console.log("ocurrio error", err)
        })
        actualizarAlias()
    }, [])

    useEffect(()=>{
        actualizarPares(aliasElegido)
    }, [aliasElegido])

    return(
            <div className="container-fluid main-container pt-0" id="main-container">
                <CabezalConfig/>
                <div className="row">
                    <div className="container">
                        <div className="row">
                            <div className="col-12 col-md-4 col-lg-3">
                                <MenuConfiguracion />
                            </div>
                            <div className="col">
                                <div className="card mb-4 shadow-sm border-0" onSubmit={handleSubmit}>
                                    <div className="card-body">
                                        <div className="row">
                                            <div className="col-12 col-md-12 col-lg-12">
                                                <div className="form-group">
                                                    <label>Alias del par</label>
                                                    <select className="form-control" name="aliasElegido" onChange={(event) => {
                                                            event.persist();
                                                            console.log("se pone nuevo alias", event.target.value)
                                                            escogerAlias( event.target.value);
                                                        }} value={aliasElegido}>
                                                        {listaAlias.map(alias=>(
                                                            <option value={alias.aliaspares_id}>{alias.aliaspares_alias}</option>
                                                        ))}
                                                    </select>
                                                </div>
                                            </div>
                                        </div>
                                    </div>
                                </div>
                                <form className="card mb-4 shadow-sm border-0" onSubmit={handleSubmit}>
                                    <div className="card-body">
                                        <div className="row">
                                            <div className="col-12 col-md-12 col-lg-4">
                                                <div className="form-group">
                                                    <label>Exchange</label>
                                                    <select className="form-control" name="exchangeID" onChange={handleInputChange} value={formulario.exchangeID}>
                                                        {listaExchanges.map(exchange=>(
                                                            <option value={exchange.exchanges_id}>{exchange.exchanges_nombre}</option>
                                                        ))}
                                                    </select>
                                                    
                                                </div>
                                            </div>
                                            <div className="col-12 col-md-12 col-lg-4">
                                                <div className="form-group">
                                                    <label>Par de entrada</label>
                                                    <input type="text" className="form-control" placeholder="Por ejemplo BITCOIN" name="parPrimario" onChange={handleInputChange} value={formulario.parPrimario}/>
                                                </div>
                                            </div>
                                            <div className="col-12 col-md-12 col-lg-4">
                                                <div className="form-group">
                                                    <label>Par de salida</label>
                                                    <input type="text" className="form-control" placeholder="Por ejemplo BITCOIN" name="parSecundario" onChange={handleInputChange} value={formulario.parSecundario}/>
                                                </div>
                                            </div>
                                        </div>
                                    </div>
                                    <div className="card-footer">
                                        <button className="btn btn-primary float-right">{formulario.parID ? "Guardar cambios en el par" : "Agregar nuevo par"}</button>
                                    </div>
                                </form>
                                <div className="card border-0 shadow-sm mb-4">
                                    <div className="card-header border-0 bg-none">
                                        <div className="row">
                                            <div className="col-12 col-md">
                                                <p class="fs15">Lista de pares a comparar</p>
                                            </div>
                                        </div>
                                    </div>
                                    <div className="card-body">
                                        <table className="table w-100 mb-0">
                                            <thead>
                                                <tr>
                                                    <th className="all">ID</th>
                                                    <th className="all">Exhange</th>
                                                    <th className="all">Par de entrada</th>
                                                    <th className="all">Par de salida</th>
                                                    <th className="all">Acciones</th>
                                                </tr>
                                            </thead>
                                            <tbody>
                                                {
                                                    listaPares.map((par, indice)=>(
                                                         <tr>
                                                            <td>{par.pares_id}</td>
                                                            <td>{par.exchanges_nombre}</td>
                                                            <td><div class="btn-success btn btn-sm">{par.pares_primario}</div></td>
                                                            <td><div class="btn-warning btn btn-sm">{par.pares_secundario}</div></td>
                                                            <td>
                                                                <button type="button" class="mb-2 shadow-sm mr-2 btn btn-link text-primary" onClick={ev=>{
                                                                    ev.preventDefault()
                                                                    editarPar(par)
                                                                }}><i class="material-icons">edit</i></button>
                                                                <button type="button" class="mb-2 shadow-sm mr-2 btn btn-link text-primary" onClick={ev=>{
                                                                    ev.preventDefault()
                                                                    if(window.confirm("¿Está seguro que desea eliminar el par "+par.pares_primario+"/"+par.pares_secundario+" de "+par.exchanges_nombre+"?")){
                                                                        eliminarPar(par.pares_id)
                                                                    }
                                                                }}><i class="material-icons">delete</i></button>
                                                            </td>
                                                        </tr>
                                                    ))
                                                }
                                                
                                            </tbody>
                                        </table>
                                    </div>
                                </div>
                            </div>
                        </div>
                    </div>
                </div>

            </div>
    )
} 