import React, {useEffect, useState} from "react";
import { useToasts } from 'react-toast-notifications'
import Peticiones from '../../servicios/Peticiones'
import MenuConfiguracion from "./menuConfiguracion"
import CabezalConfig from "./cabezalConfig"

export default function ConfiguracionAlias(){
    const [formulario, ponerFormulario] = useState({});
    const [listaAlias, ponerAlias] = useState([]);
    const { addToast } = useToasts()
    const handleInputChange = (event) => {
        event.persist();
        ponerFormulario(formulario => ({...formulario, [event.target.name]: event.target.value}));
    }
    const actualizarAlias = () =>{
        Peticiones.get("obtenerAlias").then(resp=>{
            console.log("traigo resp alias", resp)
            if(resp.data.success==1){
                ponerAlias(resp.data.alias)
            }
        }, err=>{
            console.log("ocurrio error", err)
        })
    }
    const handleSubmit = (ev) => {
        ev.preventDefault();
        Peticiones.post("registrarAliasPar", formulario).then(resp=>{
            ponerFormulario({
                alias: ""
            })
            addToast("Los cambios han sido guardados correctamente.", { appearance: 'success', autoDismiss: true, pauseOnHover: true })
            actualizarAlias()
        }, err=>{
            console.log("ocurrio error", err)
        })
    }
    const editarAlias = (aliasElegido =>{
        ponerFormulario({
            aliasID: aliasElegido.aliaspares_id,
            alias: aliasElegido.aliaspares_alias
        })
    })
    const eliminarAlias = (aliasID) => {
        Peticiones.post("eliminarAlias", {
            aliasID
        }).then(resp=>{
            addToast("El alias ha sido eliminado correctamente", { appearance: 'success', autoDismiss: true, pauseOnHover: true })
            actualizarAlias()
        }, err=>{
            console.log("ocurrio error", err)
        })
    }
    useEffect(() => {
        console.log("inicia use effect")
        actualizarAlias()
    }, [])

    return(
            <div className="container-fluid main-container pt-0" id="main-container">
                <CabezalConfig/>
                <div className="row">
                    <div className="container">
                        <div className="row">
                            <div className="col-12 col-md-4 col-lg-3">
                                <MenuConfiguracion />
                            </div>
                            <div className="col">
                                <form className="card mb-4 shadow-sm border-0" onSubmit={handleSubmit}>
                                    <div className="card-body">
                                        <div className="row">
                                            <div className="col-12 col-md-12 col-lg-12">
                                                <div className="form-group">
                                                    <label>Alias del par</label>
                                                    <input type="text" className="form-control" placeholder="Por ejemplo BITCOIN" name="alias" onChange={handleInputChange} value={formulario.alias}/>
                                                </div>
                                            </div>
                                        </div>
                                    </div>
                                    <div className="card-footer">
                                        <button className="btn btn-primary float-right">Guardar cambios</button>
                                    </div>
                                </form>
                                <div class="card border-0 shadow-sm mb-4">
                                    <div class="card-header border-0 bg-none">
                                        <div class="row">
                                            <div class="col-12 col-md">
                                                
                                            </div>
                                            <div class="col-auto align-self-center">
                                                <button class="btn btn-sm btn-outline-template ml-2">
                                                    <i class="material-icons md-18">cloud_download</i> Nuevo alias
                                                </button>
                                            </div>
                                        </div>
                                    </div>
                                    <div class="card-body">
                                        <table class="table w-100 mb-0">
                                            <thead>
                                                <tr>
                                                    <th className="all">ID</th>
                                                    <th className="all">Alias</th>
                                                    <th className="all">Acciones</th>
                                                </tr>
                                            </thead>
                                            <tbody>
                                                {
                                                    listaAlias.map((alias, indice)=>(
                                                         <tr>
                                                            <td>{alias.aliaspares_id}</td>
                                                            <td>
                                                                <div class="btn-success btn btn-sm">{alias.aliaspares_alias}</div>
                                                            </td>
                                                            <td>
                                                                <button type="button" class="mb-2 shadow-sm mr-2 btn btn-link text-primary" onClick={ev=>{
                                                                    ev.preventDefault()
                                                                    editarAlias(alias)
                                                                }}><i class="material-icons">edit</i></button>
                                                                <button type="button" class="mb-2 shadow-sm mr-2 btn btn-link text-primary" onClick={ev=>{
                                                                    ev.preventDefault()
                                                                    if(window.confirm("¿Está seguro que desea eliminar el alias "+alias.aliaspares_alias+"?")){
                                                                        eliminarAlias(alias.aliaspares_id)
                                                                    }
                                                                }}><i class="material-icons">delete</i></button>
                                                            </td>
                                                        </tr>
                                                    ))
                                                }
                                                
                                            </tbody>
                                        </table>
                                    </div>
                                </div>
                            </div>
                        </div>
                    </div>
                </div>

            </div>
    )
} 