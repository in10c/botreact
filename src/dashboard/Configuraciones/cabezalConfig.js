import React from "react"
import backgroundImage from '../../assets/img/background-part.png';
import logotipo from '../../assets/img/avatar.jpeg';

export default function CabezalConfig(){

    return(<React.Fragment>
            <div className="row">
                <div className="position-relative w-100 h-320">
                    <div className="background">
                        <img src={backgroundImage} alt="" />
                    </div>
                </div>
            </div>

            <div className="row">
                <div className="container">
                    <div className="row top-100">
                        <div className="col-12 col-md-4 col-lg-3 pb-4">
                            <div className="row">
                                <div className="col-12 text-center">
                                    <figure className="avatar avatar-180 rounded-circle shadow  mx-auto">
                                        <img src={logotipo} alt="" />
                                    </figure>
                                </div>
                            </div>
                        </div>
                        <div className="col my-3">
                            <h4 className="text-white">Salvador Medina</h4>
                            <p className="text-mute text-white">CEO Corporativo Escrypser
                            </p>
                        </div>
                    </div>
                </div>
            </div>
        </React.Fragment>
    )
}