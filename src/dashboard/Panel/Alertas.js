import React, { useEffect, useState } from 'react';
import Peticiones from '../../servicios/Peticiones'
import {
    NavLink
} from "react-router-dom";
import numeral from "numeral"

export default function Alertas(){
    const [listaAlertas, ponerAlertas] = useState([]);
    useEffect(()=>{
        // console.log("se ejecuta alias")
        Peticiones.get("obtenerAlertas").then(resp=>{
            console.log("de ontemer alertas", resp)
            if(resp.data.success==1){
                ponerAlertas(resp.data.alertas)
            }
        }, err=>{
            console.log("ocurrio error", err)
        })
    }, [])


    return (
        <React.Fragment>
        <div className="container main-container" id="main-container" style={{marginTop: 30}}>
                <div className="card border-0 shadow-sm mb-4">
                    <div className="card-header border-0 bg-none">
                        <div className="row">
                            <div className="col-12 col-md">
                                <p className="fs15">Alertas de arbitraje</p>
                            </div>
                        </div>
                    </div>
                    <div className="card-body">
                            <table className="table w-100 mb-0">
                                <thead>
                                    <tr>
                                        <th className="all">Alias</th>
                                        <th className="all">Exchange origen</th>
                                        <th className="all">Exchange destino</th>
                                        <th className="all">Precio compra</th>
                                        <th className="all">Precio venta </th>
                                        <th className="all">Diferencia</th>
                                    </tr>
                                </thead>
                                <tbody>
                                    {
                                        listaAlertas.map(alerta=>(<tr>
                                            <td>
                                                <NavLink to={"/transaccion/"+alerta.diferencias_exchangeorigen+"/"+alerta.diferencias_exchangedestino+"/"+alerta.diferencias_aliaspares_id}>
                                                    {alerta.aliaspares_alias}
                                                </NavLink>
                                            </td>
                                            <td>{alerta.exchangeorigen}</td>
                                            <td>{alerta.exchangedestino}</td>
                                            <td>{alerta.diferencias_origenask}</td>
                                            <td>{alerta.diferencias_destinobid}</td>
                                            <td>{alerta.diferencias_diferencia}</td>
                                        </tr>))
                                    }
                                </tbody>
                            </table>
                    </div>
                </div>
        </div>
        </React.Fragment>
    );
    
}