import React, { useEffect, useState } from 'react';
import Peticiones from '../../servicios/Peticiones'
import {
    NavLink
} from "react-router-dom";
import numeral from "numeral"

export default function Comparativa(){
    const [configuracion, ponerConfig] = useState({})
    const [listaAlias, ponerAlias] = useState([]);
    const [aliasID, ponerAliasID] = useState([]);
    const [listaInfos, ponerInfo] = useState([]);
    const [cargando, ponerCargando] = useState(true);
    useEffect(()=>{
        console.log("se ejecuta alias")
        Peticiones.get("obtenerConfiguracion").then(resp=>{
            console.log("traigo respuesta", resp)
            ponerConfig({
                porcentajeMinimo: resp.data.configuracion.configuracion_porcentajeminimo,
                montoOperacion: resp.data.configuracion.configuracion_montooperacion
            })
        }, err=>{
            console.log("ocurrio error", err)
        })
        Peticiones.get("obtenerAlias").then(resp=>{
            console.log("traigo respuesta", resp)
            if(resp.data.success==1){
                ponerAlias(resp.data.alias)
            }
            ponerCargando(false)
        }, err=>{
            console.log("ocurrio error", err)
        })
    }, [])

    useEffect(()=>{
        ponerCargando(true)
        Peticiones.post("obtenerDatosPar", {aliasID}).then(resp=>{
            console.log("traigo respuesta de datos par", resp)
            if(resp.data.success==1){
                ponerInfo(resp.data.infos)
                ponerCargando(false)
            }
        }, err=>{
            console.log("ocurrio error", err)
        })
    }, [aliasID])

    const regresaMatrizContenido = (parActual) => {
        let matriz = []
        listaInfos.map(info=>{
            if(info.exchange != parActual.exchange){
                let porcentaje = ( 1 - (parActual.ask / info.bid ) ) * 100
                matriz.push({porcentaje, exchange: info.exchange, exchangeEmisorID: parActual.exchangeID,exchangeReceptorID: info.exchangeID})
            }
        })
        return matriz
    }
    return (
        <React.Fragment>
        <div class="loader container-fluid" style={{display: cargando ? "block" : "none"}}>
            <div class="row h-100">
                <div class="col-auto align-self-center  mx-auto text-center">
                    <div class="loader-ripple"><div></div><div></div></div>
                    <h2>Cargando</h2>
                    <p>Por favor espere...</p>
                </div>
            </div>
        </div>
        <div className="submenu">
            <div className="container-fluid " id="submenu-container">
                <div className="row" style={{paddingTop: 12}}>
                    <div className="col-md-10">
                        <div class="form-group ">
                            <label>Seleccionar alias de pares</label>
                            <select class="form-control" value={aliasID} onChange={event=>{
                                event.persist();
                                ponerAliasID(event.target.value);
                            }}>
                                <option value={-1} selected>Selecciona uno de la lista...</option>
                                {listaAlias.map(alias=>(<option value={alias.aliaspares_id}>{alias.aliaspares_alias}</option>))}
                            </select>
                        </div>
                    </div>
                </div>
            </div>
        </div>
        <div className="container main-container" id="main-container">
            <div className="row">
                {listaInfos.map(infoExchange => (<div className="col-12 col-md-6 col-lg-6 col-xl-4">
                    <div className="card border-0 shadow-sm mb-4">
                        <div className="background-half text-template-primary-opac-10 h-200" />
                        <div className="card-header border-0 bg-none">
                        <div className="row">
                            <div className="col">
                            <p className="fs15">{infoExchange.exchange}<br /><small className="text-template-primary-light">Volumen de par en exchange: {numeral(infoExchange.volume).format('$0,0.00')}</small></p>
                            </div>
                            <div className="col-auto">

                            </div>
                        </div>
                        </div>
                        <div className="card-body text-center">
                        <p className="text-template-primary-light small mb-2">Precio del par</p>
                        <h4 className="font-weight-light mb-4"><small>$</small>{infoExchange.bid} | <small>$</small>{infoExchange.ask}</h4>
                        <div className="row mt-3">
                            <div className="col-12 col-md-11 mx-auto mw-300">
                            <div className="row">
                                {regresaMatrizContenido(infoExchange).map(compara=> (
                                    <div className="col-6">
                                        <div className="card border-0 shadow mb-4">
                                            <NavLink to={"/transaccion/"+compara.exchangeEmisorID+"/"+compara.exchangeReceptorID+"/"+aliasID}>
                                                <div className="card-body text-center">
                                                    {compara.porcentaje > configuracion.porcentajeMinimo ? <i className="material-icons">notifications_active</i> : <i className="material-icons">notifications_none</i>}
                                                    <p className="text-template-primary-light small mb-1">{compara.exchange}</p>
                                                        <h6 className="mb-0">{compara.porcentaje.toFixed(3)}%</h6>
                                                </div>
                                            </NavLink>
                                        </div>
                                    </div>
                                ))}
                            </div>
                            </div>
                        </div>
                        </div>
                    </div>
                </div>))}
            </div>
        </div>
        </React.Fragment>
    );
    
}