import React from 'react';
import { Chart } from "react-chartjs-2";
import Peticiones from '../../../servicios/Peticiones'

import './Mixbarcharts.css';

class Mixbarcharts extends React.Component {
    constructor(props) {
        super(props);
    }
    chartRef = React.createRef();

    componentDidMount() {
        const myChartRef = this.chartRef.current.getContext("2d");
        console.log("al montar chart ", this.props)
        Peticiones.post("historialDiferencias", this.props.config).then(resp=>{
            console.log("traigo respuesta", resp)
            if(resp.data.success==1){
                // diferencias_id: 449
                // diferencias_aliaspares_id: 24
                // diferencias_exchangeorigen: 7
                // diferencias_exchangedestino: 1
                // diferencias_diferencia: 99.82440076506734
                // diferencias_origenbid: 0.0004307
                // diferencias_origenask: 0.0004315
                // diferencias_destinobid: 0.24573
                // diferencias_destinoask: 0.24574
                // diferencias_tiempo: "2020-01-18T20:36:01.000Z"
                let labels = []
                let data = []
                resp.data.diferencias.map(dif=>{
                    labels.push(dif.diferencias_tiempo)
                    data.push(dif.diferencias_diferencia)
                })
                new Chart(myChartRef, {
                    type: 'line',
                    data: {
                        labels,
                        datasets: [{
                            label: 'Diferencia',
                            borderWidth: '2',
                            borderColor: '#5B92FF',
                            backgroundColor: 'rgba(0, 0, 0, 0)',
                            data
                            }]
                    },
                    options: {
                        responsive: true,
                        maintainAspectRatio: false,
                        elements: {
                            point: {
                                radius: 10
                            }
                        },
                        title: {
                            display: false,
                            text: 'Chart.js Line Chart - Stacked Area'
                        },
                        tooltips: {
                            enabled: true,
                            mode: 'nearest',
                        },
                        hover: {
                            mode: 'index'
                        },
                        legend: {
                            display: false,
                        },
                        scales: {
                            xAxes: [{
                                ticks: {
                                    display: true,
                                    fontColor: "#90b5ff",
                                },
                                scaleLabel: {
                                    display: false,
                                    labelString: 'Month'
                                }
                                }],
                            yAxes: [{
                                ticks: {
                                    display: true,
                                    fontColor: "#90b5ff",
                                },
                                display: true,
                                stacked: true,
                                scaleLabel: {
                                    display: false,
                                    labelString: 'Value'
                                }
                                }]
                        }
                    }
                });
            }
        }, err=>{
            console.log("ocurrio error", err)
        })
        
      }
    
      render() {
        return (     
            <canvas id="mixedbarchartjs" ref={this.chartRef} height="220"></canvas>     
        );
      }
}

export default Mixbarcharts;