import React, {useEffect, useState} from "react"
import Peticiones from '../../servicios/Peticiones'
import { useToasts } from 'react-toast-notifications'
import { useParams} from "react-router";
import Mixbarcharts from './Mixbarcharts/Mixbarcharts.react';

export default function PreviaTransaccion(){
    const parametros = useParams();
    const { addToast } = useToasts()
    const [emisor, ponerEmisor] = useState({})
    const [tarifas, ponerTarifas] = useState([])
    const [inversion, ponerInversion] = useState(1000)
    const [montoSalida, ponerSalida] = useState(0)
    const [montoFee, ponerFee] = useState(0)
    const [receptor, ponerReceptor] = useState({})

    const obtenerDiferenciaPrecios = () =>{
        Peticiones.post("diferenciaPrecios", parametros).then(resp=>{
            
            if(resp.data.success==1){
                ponerEmisor(resp.data.emisor)
                //ponerInversion(resp.data.emisor.cantidadEnWalletRetiro)
                ponerReceptor(resp.data.receptor)
            }
            console.log("traigo respuesta", resp)
        }, err=>{
            console.log("ocurrio error", err)
        })
        // setTimeout(()=>{
        //     console.log("obtengo nuevamente diferencia de precios")
        //     obtenerDiferenciaPrecios()
        // }, 60000)
    }
    const obtenerImpuestos = () =>{
        let aux = JSON.parse(JSON.stringify(parametros))
        aux.inversion = inversion
        aux.precioEmisor = emisor.ask
        aux.precioReceptor = receptor.bid
        Peticiones.post("obtenerImpuestos", aux).then(resp=>{
            console.log("traigo de obtener impuestos", resp)
            if(resp.data.success==1){
                ponerTarifas(resp.data.tarifas)
                let comisionesTotal = 0
                resp.data.tarifas.map(tarif=>{
                    comisionesTotal+= tarif.tarifa
                })
                ponerFee(comisionesTotal)
                ponerSalida(resp.data.tarifas[3].salida)
            } else {
                addToast("Ocurrió un error al traer las cuotas: \n"+resp.data.message, { appearance: 'error', autoDismiss: true, pauseOnHover: true })
            }
        }, err=>{
            console.log("ocurrio error", err)
        })
    }

    const procesarTransaccion = () =>{
        // emisorExchange: "3"
        // receptorExchange: "2"
        // alias: "1"
        let aux = JSON.parse(JSON.stringify(parametros))
        aux.cantidad = inversion
        aux.precioEmisor = emisor.price
        aux.precioReceptor = receptor.price
        aux.comisiones = montoFee
        aux.salida = montoSalida
        Peticiones.post("procesarTransaccion", aux).then(resp=>{
            console.log("traigo de obtener impuestos", resp)
            if(resp.data.success==1){
                
            } else {
                addToast("Ocurrió un error al procesar la transacción: \n"+resp.data.message, { appearance: 'error', autoDismiss: true, pauseOnHover: true })
            }
        }, err=>{
            console.log("ocurrio error", err)
        })
    }
    useEffect(()=>{
        obtenerDiferenciaPrecios()
    }, [])
    return ( 
        <div className="container-fluid main-container pt-0" id="main-container">
            <div className="page-subtitle">Traspaso de criptoactivos <strong>{emisor.exchange}</strong> A <strong>{receptor.exchange}</strong></div>
            <div className="card text-left shadow-sm border-0 mb-4">
                <div className="card-body ">
                    <div className="row">
                        
                        <div className="col-12 col-md-8 col-lg-9">
                            <div class="row">
                                <div class="col-auto">
                                    <div class="row">
                                        <div class="col" style={{fontSize:23}}>
                                            <p class="subtitle mb-1 text-mute">{emisor.exchange}</p>
                                            <h6 class="mb-0"  style={{fontSize:27}}>compra: ${emisor.ask}</h6>
                                            <h6 class="mb-0"  style={{fontSize:27}}>venta: ${emisor.bid}</h6>
                                            <p class="text-danger">{(( 1 - (receptor.ask / emisor.bid ) ) * 100).toFixed(3)}%</p>
                                        </div>
                                    </div>
                                </div>
                                <div class="col-auto border-left-dashed">
                                    <div class="row">
                                        <div class="col" style={{fontSize:23}}>
                                            <p class="subtitle mb-1 text-mute">{receptor.exchange}</p>
                                            <h6 class="mb-0"  style={{fontSize:27}}>compra: ${receptor.ask}</h6>
                                            <h6 class="mb-0"  style={{fontSize:27}}>venta: ${receptor.bid}</h6>
                                            <p class="text-success">{(( 1 - (emisor.ask / receptor.bid ) ) * 100).toFixed(3)}%</p>
                                        </div>
                                    </div>
                                </div>
                            </div>
                            <div className="row" style={{marginTop:25}}>
                                <div className="col-md-12">
                                <table class="table table-bordered w-100 mb-0">
                                    <thead>
                                        <tr>
                                            <th class="all">Paso</th>
                                            <th class="all">Exchange</th>
                                            <th class="all">Descripción</th>
                                            <th class="all">Primaria</th>
                                            <th class="all">Secundaria </th>
                                            <th class="all">Tasa </th>
                                            <th class="all">Tarifa </th>
                                            <th class="all">Salida </th>
                                            <th></th>
                                        </tr>
                                    </thead>
                                    <tbody>
                                        {tarifas.map(tarifa=>(<tr>
                                            <td>{tarifa.paso}</td>
                                            <td>{tarifa.nombreExchange}</td>
                                            <td>{tarifa.detalles}</td>
                                            <td>{tarifa.cantidadPrincipal}</td>
                                            <td>{tarifa.cantidadSecundaria}</td>
                                            <td>{tarifa.tasa}</td>
                                            <td>{tarifa.tarifa}</td>
                                            <td>{tarifa.salida}</td>
                                        </tr>))}
                                        
                                    </tbody>
                                </table>
                                </div>
                            </div>
                        </div>
                        <div className="col-12 col-md-12 col-lg-3 text-left ">
                            <h5>Porcentaje de ganancia: </h5>
                            <p className="text-default mb-0"  style={{fontSize:22}}>{(( 1 - (emisor.ask / receptor.bid ) ) * 100).toFixed(3)}%</p>
                            <h5>A recibir esperado: </h5>
                            <p className="text-default mb-0"  style={{fontSize:22}}>${(inversion * ( 1 + ( 1 - (emisor.ask / receptor.bid )  ))).toFixed(3)}</p>
                            <h5>Total cuotas: </h5>
                                    <p className="text-default mb-0"  style={{fontSize:22}}>${montoFee}</p>
                            <h5>A recibir menos cuotas: </h5>
                                    <p className="text-success mb-0"  style={{fontSize:22}}>${montoSalida}</p>
                            <hr />
                            <div className="row">
                                <div className="col-12">
                                    <div class="form-group " style={{marginTop:20}}>
                                        <h4 style={{fontSize: 17}}>Cantidad a enviar:</h4>
                                        <div class="input-group">
                                            <div class="input-group-prepend">
                                                <span class="input-group-text">$</span>
                                            </div>
                                            <input type="text" class="form-control" value={inversion} onChange={(event) => {
                                                event.persist();
                                                ponerInversion(event.target.value);
                                            }} aria-label="Amount (to the nearest dollar)" />
                                            <div class="input-group-append">
                                                <span class="input-group-text">.00</span>
                                            </div>
                                        </div>
                                    </div>
                                    <button className="btn btn-primary btn-block mt-2" onClick={obtenerImpuestos}>Sacar impuestos</button>
                                    {//<button className="btn btn-primary btn-block mt-2" onClick={procesarTransaccion}>Procesar transacción</button>
                                    }
                                </div>
                            </div>
                        </div>
                    </div>
                </div>
            </div>
            <div className="page-subtitle">Historial de diferencias de precios entre los exchanges</div>
            <div className="card border-0 shadow-sm mb-4">
                    <div className="card-body">
                        <div className="areachart">
                            <Mixbarcharts config={parametros}></Mixbarcharts>
                        </div>
                    </div>
                </div>
        </div>
    )
}
