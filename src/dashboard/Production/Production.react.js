import React from 'react';
import { Tab, Nav, Dropdown, Button } from 'react-bootstrap';
import Barcharts from './Barcharts/Barcharts.react';
import Summaryone from './Summaryone/Summaryone.react';
import Summarytwo from './Summarytwo/Summarytwo.react';
import Circularprogress from './Circularprogress/Circularprogress.react';
import Dougnutchart from './Dougnutchart/Dougnutchart.react';
import Linechart from './Linechart/Linechart.react';
import Barchartviews from './Barchartviews/Barchartviews.react';
import Vectormap from './Vectormap/Vectormap.react';

import userimage1 from '../../assets/img/user1.png';
import userimage2 from '../../assets/img/user2.png';
import userimage3 from '../../assets/img/user3.png';
import userimage4 from '../../assets/img/user4.png';
import userimage5 from '../../assets/img/user5.png';

import newsimage1 from '../../assets/img/newzimage.png';

var backgroundnews = {
    backgroundImage : 'url('+ newsimage1 +')'
};

class Production extends React.Component {
    render() {
        return (
            <div className="container main-container" id="main-container">
                {/* Main container starts */}
           
                <div className="row">
                    <div className="col-12 col-md-6 col-lg-6 col-xl-3">
                    <div className="card border-0 shadow-sm overflow-hidden mb-4">
                        <div className="card-body py-0">
                        <div className="row">
                            <div className="col py-2 pr-0">
                            <p>Total Profit</p><br />
                            <h4 className="font-weight-light mb-0"><small>$</small> 25,410</h4>
                            </div>
                            <div className="col-auto bg-primary  text-white py-2">
                            <p className="mb-0"><small>+20%<span className="text-mute d-block">Quarter 3</span></small></p>
                            <div className="summary-one">
                                 <Summaryone/>   
                            </div>
                         </div>
                        </div>
                         </div>
                    </div>
                    </div>
                    <div className="col-12 col-md-6 col-lg-6 col-xl-3">
                    <div className="card border-0 shadow-sm overflow-hidden mb-4">
                        <div className="card-body py-0">
                        <div className="row">
                            <div className="col py-2 pr-0">
                            <p>Total Loss</p><br />
                            <h4 className="font-weight-light mb-0"><small>$</small> 15,410</h4>
                            </div>
                            <div className="col-auto bg-danger  text-white py-2">
                            <p className="mb-0"><small>-20%<span className="text-mute d-block">Oil Selling</span></small></p>
                            <div className="summary-one">
                                <Summarytwo/>
                                </div>  
                            </div>
                        </div>
                        </div>
                    </div>
                    </div>
                    <div className="col-12 col-md-6 col-lg-6 col-xl-3">
                        <div className="card border-0 shadow-sm overflow-hidden mb-4">
                            <div className="card-body py-0">
                                <div className="row">
                                    <div className="col py-2">
                                    <p>Awareness<br /><span className="text-template-primary">Frequency</span></p>
                                    <h4 className="font-weight-light mb-0">76.98%</h4>
                                    </div>
                                    <div className="col-auto pl-0 py-2 align-self-center">
                                    <div className="progress-summary">
                                        <p className="mb-0">+75.98%<br /><small><span className="text-mute d-block small">Male-Female</span></small></p>
                                        <Circularprogress/>
                                    </div>
                                    </div>
                                </div>
                            </div>
                        </div>
                    </div>
                    <div className="col-12 col-md-6 col-lg-6 col-xl-3">
                        <div className="card border-0 shadow-sm overflow-hidden mb-4">
                            <div className="card-body py-0">
                                <div className="row">
                                    <div className="col py-2 pr-0">
                                        <p>Total Deals</p><br />
                                        <h4 className="font-weight-light mb-0">15410</h4>
                                    </div>
                                    <div className="col-auto border-left-dashed py-2">
                                        <p className="mb-0"><small>15%<span className="text-mute d-block">Oil Selling</span></small></p>
                                        <div style={{height:'50px', width:'80px'}}><Summaryone></Summaryone></div>                                        
                                    </div>
                                </div>
                            </div>
                        </div>
                    </div>
                </div>
                <div className="row">
                    <div className="col-12 col-md-6 col-lg-6 col-xl-4">
                    <div className="card border-0 shadow-sm mb-4">
                        <div className="background-half text-template-primary-opac-10 h-200" />
                        <div className="card-header border-0 bg-none">
                        <div className="row">
                            <div className="col">
                            <p className="fs15">Order Summary<br /><small className="text-template-primary-light">Update about sales, payment and tickets</small></p>
                            </div>
                            <div className="col-auto">
                            <Dropdown className="dropdown">
                                <Dropdown.Toggle className="btn dropdown-toggle btn-sm btn-link" type="button" variant="link">
                                <i className="material-icons">more_horiz</i>
                                </Dropdown.Toggle>
                                <Dropdown.Menu className="dropdown-menu dropdown-menu-sm dropdown-menu-right" alignRight>
                                <a className="dropdown-item" href="#">Action</a>
                                <a className="dropdown-item" href="#">Another</a>
                                <a className="dropdown-item" href="#">Something</a>
                                </Dropdown.Menu>
                            </Dropdown>
                            </div>
                        </div>
                        </div>
                        <div className="card-body text-center">
                        <p className="text-template-primary-light small mb-2">Order Completed</p>
                        <h4 className="font-weight-light mb-4"><small>$</small> 25,410</h4>
                        <div className="row mt-3">
                            <div className="col-12 col-md-11 mx-auto mw-300">
                            <div className="row">
                                <div className="col-6">
                                <div className="card border-0 shadow mb-4">
                                    <div className="card-body text-center">
                                    <i className="material-icons">shopping_basket</i>
                                    <p className="text-template-primary-light small mb-1">Received </p>
                                    <h6 className="mb-0">15,280</h6>
                                    </div>
                                </div>
                                </div>
                                <div className="col-6">
                                <div className="card border-0 shadow mb-4">
                                    <div className="card-body text-center">
                                    <i className="material-icons">event_available</i>
                                    <p className="text-template-primary-light small mb-1">Accepted</p>
                                    <h6 className="mb-0">5,410</h6>
                                    </div>
                                </div>
                                </div>
                                <div className="col-6">
                                <div className="card border-0 shadow mb-3">
                                    <div className="card-body text-center">
                                    <i className="material-icons">local_shipping</i>
                                    <p className="text-template-primary-light small mb-1">On the way</p>
                                    <h6 className="mb-0">3,120</h6>
                                    </div>
                                </div>
                                </div>
                                <div className="col-6">
                                <div className="card border-0 shadow mb-3">
                                    <div className="card-body text-center">
                                    <i className="material-icons">error_outline</i>
                                    <p className="text-template-primary-light small mb-1">Reported</p>
                                    <h6 className="mb-0">410</h6>
                                    </div>
                                </div>
                                </div>
                            </div>
                            </div>
                        </div>
                        </div>
                    </div>
                    </div>
                    <div className="col-12 col-md-6 col-lg-6 col-xl-4 mb-4">
                    <div className="card border-0 shadow-sm h-100">
                        <div className="card-header border-0 bg-none">
                        <div className="row">
                            <div className="col">
                            <p className="fs15">Sales<br /><small className="text-template-primary-light">Per Quarter</small></p>
                            </div>
                            <div className="col-auto">
                            <Dropdown className="dropdown">
                                <Dropdown.Toggle className="btn dropdown-toggle btn-sm btn-link" type="button" variant="link">
                                <i className="material-icons">more_horiz</i>
                                </Dropdown.Toggle>
                                <Dropdown.Menu className="dropdown-menu dropdown-menu-sm dropdown-menu-right" alignRight>
                                <a className="dropdown-item" href="#">Action</a>
                                <a className="dropdown-item" href="#">Another</a>
                                <a className="dropdown-item" href="#">Something</a>
                                </Dropdown.Menu>
                            </Dropdown>
                            </div>
                        </div>
                        </div>
                        <div className="card-body">
                            <div className="areachart">
                                <Barcharts/>
                            </div>
                        <div className="row">
                            <div className="col-6 text-right">
                            <p className="mb-1">Total Sales <i className="material-icons text-template-primary fs15 vm">album</i></p>
                            <p className="text-template-primary-light">25600</p>
                            </div>
                            <div className="col-6 border-left-dashed">
                            <p className="mb-1"><i className="material-icons text-template-primary-light fs15 vm">album</i> Referral Sales</p>
                            <p className="text-template-primary-light">6500</p>
                            </div>
                        </div>
                        </div>
                    </div>
                    </div>
                    <div className="col-12 col-md-6 col-lg-6 col-xl-4">
                    <div className="card border-0 shadow-sm mb-4">
                        <div className="card-header border-0 bg-none">
                        <div className="row">
                            <div className="col align-self-center">
                            <p className="fs15">Sales by Categories</p>
                            </div>
                            <div className="col-auto">
                            <Dropdown className="dropdown">
                                <Dropdown.Toggle className="btn dropdown-toggle btn-sm btn-link" type="button" variant="link">
                                <i className="material-icons">more_horiz</i>
                                </Dropdown.Toggle>
                                <Dropdown.Menu className="dropdown-menu dropdown-menu-sm dropdown-menu-right" alignRight>
                                <a className="dropdown-item" href="#">Action</a>
                                <a className="dropdown-item" href="#">Another</a>
                                <a className="dropdown-item" href="#">Something</a>
                                </Dropdown.Menu>
                            </Dropdown>
                            </div>
                        </div>
                        </div>
                        <div className="card-body pt-2">
                        <div className="row">
                            <div className="col-auto text-center align-self-center">
                            <div className="donughtchart">
                                <Dougnutchart></Dougnutchart>
                            </div>
                            </div>
                            <div className="col">
                            <div className="row mb-1">
                                <div className="col-auto">
                                <i className="material-icons text-template-primary fs15 vm">album</i>
                                </div>
                                <div className="col pl-0">
                                <p>Referral Sale <small className="d-block"><span className="text-template-primary">2548</span> <span className="text-danger">20%</span></small></p>
                                </div>
                            </div>
                            <div className="row mb-1">
                                <div className="col-auto">
                                <i className="material-icons text-success fs15 vm">album</i>
                                </div>
                                <div className="col pl-0">
                                <p>Direct Sales <small className="d-block"><span className="text-template-primary">2548</span> <span className="text-success">15%</span></small></p>
                                </div>
                            </div>
                            <div className="row">
                                <div className="col-auto">
                                <i className="material-icons text-danger fs15 vm">album</i>
                                </div>
                                <div className="col pl-0">
                                <p>Offline Sales <small className="d-block"><span className="text-template-primary">2548</span> <span className="text-success">18%</span></small></p>
                                </div>
                            </div>
                            </div>
                        </div>
                        </div>
                    </div>
                    <div className="card border-0 shadow-sm mb-4">
                        <div className="card-header border-0 bg-none">
                        <div className="row">
                            <div className="col align-self-center">
                            <p className="fs15">Bounce Rate</p>
                            </div>
                            <div className="col-auto">
                            <Dropdown className="dropdown">
                                <Dropdown.Toggle className="btn dropdown-toggle btn-sm btn-link" type="button" variant="link">
                                <i className="material-icons">more_horiz</i>
                                </Dropdown.Toggle>
                                <Dropdown.Menu className="dropdown-menu dropdown-menu-sm dropdown-menu-right" alignRight>
                                <a className="dropdown-item" href="#">Action</a>
                                <a className="dropdown-item" href="#">Another</a>
                                <a className="dropdown-item" href="#">Something</a>
                                </Dropdown.Menu>
                            </Dropdown>
                            </div>
                        </div>
                        </div>
                        <div className="card-body">
                        <div className="row">
                            <div className="col-12 text-center align-self-center">
                            <div className="linechart-large">
                                <Linechart></Linechart>
                            </div>
                            </div>
                        </div>
                        </div>
                    </div>
                    </div>
                    <div className="col-12 col-md-6 col-lg-6 col-xl-4  mb-4">
                        <div className="card border-0 shadow-sm overflow-hidden h-100">
                            <div className="card-header border-0 bg-none">
                                <div className="row">
                                    <div className="col">
                                    <p className="fs15">Page Views<br /><small className="text-template-primary-light">This week vs Last week</small></p>
                                    </div>
                                    <div className="col-auto">
                                    <Dropdown className="dropdown">
                                        <Dropdown.Toggle className="btn dropdown-toggle btn-sm btn-link" type="button" variant="link">
                                        <i className="material-icons">more_horiz</i>
                                        </Dropdown.Toggle>
                                        <Dropdown.Menu className="dropdown-menu dropdown-menu-sm dropdown-menu-right" alignRight>
                                        <a className="dropdown-item" href="#">Action</a>
                                        <a className="dropdown-item" href="#">Another</a>
                                        <a className="dropdown-item" href="#">Something</a>
                                        </Dropdown.Menu>
                                    </Dropdown>
                                    </div>
                                </div>
                            </div>
                            <Tab.Container  defaultActiveKey="tabs1" >
                                <div className="card-body">
                                    <Tab.Content className="tab-content" id="myTabContent">
                                        <Tab.Pane className="tab-pane fade" eventKey="tabs1">
                                            <div className="barcharts">
                                                <Barchartviews></Barchartviews>
                                            </div>
                                        </Tab.Pane>
                                        <Tab.Pane className="tab-pane fade" eventKey="tabs2">..Tab 2..</Tab.Pane>
                                        <Tab.Pane className="tab-pane fade" eventKey="tabs3">..Tab 3..</Tab.Pane>
                                    </Tab.Content>
                                </div>
                                <div className="card-footer p-0 border-0 bg-none">
                                    <Nav className="nav nav-tabs nav-fill" id="myTab" role="tablist">
                                        <Nav.Item className="nav-item">
                                        <Nav.Link className="nav-link" eventKey="tabs1">
                                            <p className="mb-1">New User</p>
                                            <p className="text-template-primary-light">2540 <span className="badge badge-success vm">+21%</span></p>
                                        </Nav.Link>
                                        </Nav.Item>
                                        <Nav.Item className="nav-item">
                                        <Nav.Link className="nav-link" eventKey="tabs2">
                                            <p className="mb-1">Returning User</p>
                                            <p className="text-template-primary-light">1540 <span className="badge badge-danger vm">-21%</span></p>
                                        </Nav.Link>
                                        </Nav.Item>
                                        <Nav.Item className="nav-item">
                                        <Nav.Link className="nav-link" eventKey="tabs3">
                                            <p className="mb-1">Referral</p>
                                            <p className="text-template-primary-light">10 <span className="badge badge-danger vm">-21%</span></p>
                                        </Nav.Link>
                                        </Nav.Item>
                                    </Nav>
                                </div>
                            </Tab.Container>
                        </div>
                    </div>
                    <div className="col-12 col-md-6 col-lg-6 col-xl-4">
                    <div className="card border-0 shadow-sm mb-4">
                        <div className="card-header border-0 bg-none">
                        <div className="row">
                            <div className="col">
                            <p className="fs15">Popular Items<br /><small className="text-template-primary-light">This week vs Last week</small></p>
                            </div>
                            <div className="col-auto">
                            <button className="btn btn-sm btn-outline-template" type="button" data-toggle="dropdown" aria-haspopup="true" aria-expanded="false">
                                View More
                            </button>
                            </div>
                        </div>
                        </div>
                        <div className="card-body pt-2">
                        <div className="row">
                            <div className="col-12 scroll-y h-310">
                            <div className="row mb-4">
                                <div className="col w-20 align-self-center text-template-primary">1</div>
                                <div className="col-auto align-self-center">
                                <figure className="avatar avatar-50"><img src={userimage1} alt="AdminuxPRO" /></figure>
                                </div>
                                <div className="col align-self-center pl-0">
                                <p className="mb-2">Appple Laptop <i className="material-icons float-right fs15 text-danger">favorite</i></p>
                                <div className="progress h-5 mb-1">
                                    <div className="progress-bar" role="progressbar" style={{width: '25%'}} aria-valuenow={25} aria-valuemin={0} aria-valuemax={100} />
                                </div>
                                <p><span className="text-template-primary-light">2540</span> <span className="badge badge-success vm">+21%</span> <span className="float-right">1560 available</span></p>
                                </div>
                            </div>
                            <div className="row mb-4">
                                <div className="col w-20 align-self-center text-template-primary">2</div>
                                <div className="col-auto align-self-center">
                                <figure className="avatar avatar-50"><img src={userimage2} alt="AdminuxPRO" /></figure>
                                </div>
                                <div className="col align-self-center pl-0">
                                <p className="mb-2">Microfax Canvas <i className="material-icons float-right fs15 text-danger">favorite</i></p>
                                <div className="progress h-5 mb-1">
                                    <div className="progress-bar" role="progressbar" style={{width: '25%'}} aria-valuenow={25} aria-valuemin={0} aria-valuemax={100} />
                                </div>
                                <p><span className="text-template-primary-light">1654</span> <span className="badge badge-success vm">+21%</span> <span className="float-right">1560 available</span></p>
                                </div>
                            </div>
                            <div className="row mb-4">
                                <div className="col w-20 align-self-center text-template-primary">3</div>
                                <div className="col-auto align-self-center">
                                <figure className="avatar avatar-50"><img src={userimage3} alt="AdminuxPRO" /></figure>
                                </div>
                                <div className="col align-self-center pl-0">
                                <p className="mb-2">Ananta Real Juice <i className="material-icons float-right fs15 text-danger">favorite</i></p>
                                <div className="progress h-5 mb-1">
                                    <div className="progress-bar" role="progressbar" style={{width: '25%'}} aria-valuenow={25} aria-valuemin={0} aria-valuemax={100} />
                                </div>
                                <p><span className="text-template-primary-light">5460</span> <span className="badge badge-success vm">+21%</span> <span className="float-right">1560 available</span></p>
                                </div>
                            </div>
                            <div className="row mb-4">
                                <div className="col w-20 align-self-center text-template-primary">4</div>
                                <div className="col-auto align-self-center">
                                <figure className="avatar avatar-50"><img src={userimage4} alt="AdminuxPRO" /></figure>
                                </div>
                                <div className="col align-self-center pl-0">
                                <p className="mb-2">Huwaii Lamp <i className="material-icons float-right fs15 text-danger">favorite</i></p>
                                <div className="progress h-5 mb-1">
                                    <div className="progress-bar" role="progressbar" style={{width: '25%'}} aria-valuenow={25} aria-valuemin={0} aria-valuemax={100} />
                                </div>
                                <p><span className="text-template-primary-light">8467</span> <span className="badge badge-success vm">+21%</span> <span className="float-right">1560 available</span></p>
                                </div>
                            </div>
                            <div className="row mb-4">
                                <div className="col w-20 align-self-center text-template-primary">5</div>
                                <div className="col-auto align-self-center">
                                <figure className="avatar avatar-50"><img src={userimage1} alt="AdminuxPRO" /></figure>
                                </div>
                                <div className="col align-self-center pl-0">
                                <p className="mb-2">Appple Laptop <i className="material-icons float-right fs15 text-danger">favorite</i></p>
                                <div className="progress h-5 mb-1">
                                    <div className="progress-bar" role="progressbar" style={{width: '25%'}} aria-valuenow={25} aria-valuemin={0} aria-valuemax={100} />
                                </div>
                                <p><span className="text-template-primary-light">1245</span> <span className="badge badge-success vm">+21%</span> <span className="float-right">1560 available</span></p>
                                </div>
                            </div>
                            <div className="row mb-4">
                                <div className="col w-20 align-self-center text-template-primary">6</div>
                                <div className="col-auto align-self-center">
                                <figure className="avatar avatar-50"><img src={userimage2} alt="AdminuxPRO" /></figure>
                                </div>
                                <div className="col align-self-center pl-0">
                                <p className="mb-2">Huwaii Lamp <i className="material-icons float-right fs15 text-danger">favorite</i></p>
                                <div className="progress h-5 mb-1">
                                    <div className="progress-bar" role="progressbar" style={{width: '25%'}} aria-valuenow={25} aria-valuemin={0} aria-valuemax={100} />
                                </div>
                                <p><span className="text-template-primary-light">2540</span> <span className="badge badge-success vm">+21%</span> <span className="float-right">1560 available</span></p>
                                </div>
                            </div>
                            </div>
                        </div>
                        </div>
                    </div>
                    </div>
                    <div className="col-12 col-md-6 col-lg-6 col-xl-4">
                    <div className="card border-0 shadow-sm mb-4 overflow-hidden">
                        <div className="card-header border-0 bg-none overlay-header">
                        <div className="row">
                            <div className="col">
                            <span className="btn btn-rounded btn-sm text-white bg-light"><i className="material-icons">ac_unit</i> Trending</span>
                            </div>
                            <div className="col-auto">
                            <Dropdown className="dropdown">
                                <Dropdown.Toggle className="btn dropdown-toggle btn-sm btn-link" type="button" variant="link">
                                <i className="material-icons">more_horiz</i>
                                </Dropdown.Toggle>
                                <Dropdown.Menu className="dropdown-menu dropdown-menu-sm dropdown-menu-right" alignRight>
                                <a className="dropdown-item" href="#">Action</a>
                                <a className="dropdown-item" href="#">Another</a>
                                <a className="dropdown-item" href="#">Something</a>
                                </Dropdown.Menu>
                            </Dropdown>
                            </div>
                        </div>
                        </div>
                        <div className="card-img h-200  position-relative">
                        <div className="background" style={backgroundnews}></div>
                        </div>
                        <div className="card-body">
                        <p className="fs15">Cycling with Recycling product and latest services with Aman Ahoja</p>
                        <p className="text-template-primary-light fs13">Lorem ipsum dolor sit amet, consectetur adipiscing elit, sed do eiusmod tempor incididunt ut labore et dolore magna aliqua. Lorem ipsum dolor sit amet, consectetur adipiscing elit, sed do eiusmod tempor incididunt ut labore et dolore magna aliqua.</p>
                        </div>
                        <div className="card-footer text-right">
                        <button className="btn btn-sm btn-outline-template" type="button" data-toggle="dropdown" aria-haspopup="true" aria-expanded="false">
                            Read More
                        </button>
                        </div>
                    </div>
                    </div>
                </div>
                <div className="row">
                    <div className="col-12 col-md-12 col-lg-12 col-xl-8 mb-4">
                    <div className="card border-0 shadow-sm overflow-hidden h-100">
                        <div className="card-header border-0 bg-none">
                        <div className="row">
                            <div className="col-12 col-md">
                            <p className="fs15">Assigned Orders and Status<br /><small className="text-template-primary-light">This week vs Last week</small></p>
                            </div>
                            <form className="form-inline search col-auto align-self-center px-0">
                            <input className="form-control form-control-sm" type="search" placeholder="Search" aria-label="Search" />
                            <button className="btn btn-link btn-sm" type="submit"><i className="material-icons">search</i></button>
                            </form>
                            <div className="col-auto align-self-center">
                            <button className="btn btn-sm btn-outline-template ml-2">
                                <i className="material-icons md-18">cloud_download</i> Export
                            </button>
                            </div>
                        </div>
                        </div>
                        <div className="card-body ">
                        <table className="table datatable display responsive w-100">
                            <thead>
                            <tr>
                                <th className="all">Order ID</th>
                                <th className="min-tablet">Order From</th>
                                <th className="min-desktop">Date</th>
                                <th >Status</th>
                                <th className="min-desktop">Order Receiver </th>
                                <th />
                            </tr>
                            </thead>
                            <tbody>
                            <tr>
                                <td>ID0001</td>
                                <td>
                                <div className="media">
                                    <figure className="mb-0 avatar avatar-40 mr-2">
                                    <img src={userimage3} alt="AdminuxPRO" />
                                    </figure>
                                    <div className="media-body">
                                    <p className="mb-0 template-inverse">John Alexandar</p>
                                    <p className="text-template-primary-light">Sydney, Australia</p>
                                    </div>
                                </div>
                                </td>
                                <td>12-12-2019</td>
                                <td>
                                <div className="btn-danger btn btn-sm">Accepted</div>
                                </td>
                                <td>
                                <div className="media">
                                    <figure className="mb-0 avatar avatar-40 mr-2">
                                    <img src={userimage1} alt="AdminuxPRO" />
                                    </figure>
                                    <div className="media-body">
                                    <p className="mb-0 template-inverse">Sonya Wilson</p>
                                    <p className="text-template-primary-light">Aquaguaard Manager</p>
                                    </div>
                                </div>
                                </td>
                                <td>
                                <Dropdown className="dropdown">
                                    <Dropdown.Toggle className="btn dropdown-toggle btn-sm btn-link" type="button" variant="link">
                                    <i className="material-icons">more_horiz</i>
                                    </Dropdown.Toggle>
                                    <Dropdown.Menu className="dropdown-menu dropdown-menu-sm dropdown-menu-right" alignRight>
                                    <a className="dropdown-item" href="#">View</a>
                                    <a className="dropdown-item" href="#">Edit</a>
                                    <a className="dropdown-item" href="#">Delete</a>
                                    </Dropdown.Menu>
                                </Dropdown>
                                </td>
                            </tr>
                            <tr>
                                <td>ID0002</td>
                                <td>
                                <div className="media">
                                    <figure className="mb-0 avatar avatar-40 mr-2">
                                    A
                                    </figure>
                                    <div className="media-body">
                                    <p className="mb-0 template-inverse">Anthony Desouza</p>
                                    <p className="text-template-primary-light">Sydney, Australia</p>
                                    </div>
                                </div>
                                </td>
                                <td>12-12-2019</td>
                                <td>
                                <div className="btn-warning btn btn-sm">Accepted</div>
                                </td>
                                <td>
                                <div className="media">
                                    <figure className="mb-0 avatar avatar-40 mr-2">
                                    <img src={userimage2} alt="AdminuxPRO" />
                                    </figure>
                                    <div className="media-body">
                                    <p className="mb-0 template-inverse">Anjali Govind</p>
                                    <p className="text-template-primary-light">Aquaguaard Manager</p>
                                    </div>
                                </div>
                                </td>
                                <td>
                                <Dropdown className="dropdown">
                                    <Dropdown.Toggle className="btn dropdown-toggle btn-sm btn-link" type="button" variant="link">
                                    <i className="material-icons">more_horiz</i>
                                    </Dropdown.Toggle>
                                    <Dropdown.Menu className="dropdown-menu dropdown-menu-sm dropdown-menu-right" alignRight>
                                    <a className="dropdown-item" href="#">View</a>
                                    <a className="dropdown-item" href="#">Edit</a>
                                    <a className="dropdown-item" href="#">Delete</a>
                                    </Dropdown.Menu>
                                </Dropdown>
                                </td>
                            </tr>
                            <tr>
                                <td>ID0003</td>
                                <td>
                                <div className="media">
                                    <figure className="mb-0 avatar avatar-40 mr-2">
                                    M
                                    </figure>
                                    <div className="media-body">
                                    <p className="mb-0 template-inverse">Mark Zukarburgs</p>
                                    <p className="text-template-primary-light">Sydney, Australia</p>
                                    </div>
                                </div>
                                </td>
                                <td>12-12-2019</td>
                                <td>
                                <div className="btn-primary btn btn-sm">Accepted</div>
                                </td>
                                <td>
                                <div className="media">
                                    <figure className="mb-0 avatar avatar-40 mr-2">
                                    <img src={userimage3} alt="AdminuxPRO" />
                                    </figure>
                                    <div className="media-body">
                                    <p className="mb-0 template-inverse">Syam Prashad</p>
                                    <p className="text-template-primary-light">Aquaguaard Manager</p>
                                    </div>
                                </div>
                                </td>
                                <td>
                                <Dropdown className="dropdown">
                                    <Dropdown.Toggle className="btn dropdown-toggle btn-sm btn-link" type="button" variant="link">
                                    <i className="material-icons">more_horiz</i>
                                    </Dropdown.Toggle>
                                    <Dropdown.Menu className="dropdown-menu dropdown-menu-sm dropdown-menu-right" alignRight>
                                    <a className="dropdown-item" href="#">View</a>
                                    <a className="dropdown-item" href="#">Edit</a>
                                    <a className="dropdown-item" href="#">Delete</a>
                                    </Dropdown.Menu>
                                </Dropdown>
                                </td>
                            </tr>
                            <tr>
                                <td>ID0004</td>
                                <td>
                                <div className="media">
                                    <figure className="mb-0 avatar avatar-40 mr-2">
                                    <img src={userimage3} alt="AdminuxPRO" />
                                    </figure>
                                    <div className="media-body">
                                    <p className="mb-0 template-inverse">John Doe</p>
                                    <p className="text-template-primary-light">Sydney, Australia</p>
                                    </div>
                                </div>
                                </td>
                                <td>12-12-2019</td>
                                <td>
                                <div className="btn-success btn btn-sm">Accepted</div>
                                </td>
                                <td>
                                <div className="media">
                                    <figure className="mb-0 avatar avatar-40 mr-2">
                                    S
                                    </figure>
                                    <div className="media-body">
                                    <p className="mb-0 template-inverse">Sonya Wilson</p>
                                    <p className="text-template-primary-light">Aquaguaard Manager</p>
                                    </div>
                                </div>
                                </td>
                                <td>
                                <Dropdown className="dropdown">
                                    <Dropdown.Toggle className="btn dropdown-toggle btn-sm btn-link" type="button" variant="link">
                                    <i className="material-icons">more_horiz</i>
                                    </Dropdown.Toggle>
                                    <Dropdown.Menu className="dropdown-menu dropdown-menu-sm dropdown-menu-right" alignRight>
                                    <a className="dropdown-item" href="#">View</a>
                                    <a className="dropdown-item" href="#">Edit</a>
                                    <a className="dropdown-item" href="#">Delete</a>
                                    </Dropdown.Menu>
                                </Dropdown>
                                </td>
                            </tr>
                            <tr>
                                <td>ID0005</td>
                                <td>
                                <div className="media">
                                    <figure className="mb-0 avatar avatar-40 mr-2">
                                    M
                                    </figure>
                                    <div className="media-body">
                                    <p className="mb-0 template-inverse">Mark Zukarburgs</p>
                                    <p className="text-template-primary-light">Sydney, Australia</p>
                                    </div>
                                </div>
                                </td>
                                <td>12-12-2019</td>
                                <td>
                                <div className="btn-success btn btn-sm">Accepted</div>
                                </td>
                                <td>
                                <div className="media">
                                    <figure className="mb-0 avatar avatar-40 mr-2">
                                    <img src={userimage4} alt="AdminuxPRO" />
                                    </figure>
                                    <div className="media-body">
                                    <p className="mb-0 template-inverse">Sonya Wilson</p>
                                    <p className="text-template-primary-light">Aquaguaard Manager</p>
                                    </div>
                                </div>
                                </td>
                                <td>
                                <Dropdown className="dropdown">
                                    <Dropdown.Toggle className="btn dropdown-toggle btn-sm btn-link" type="button" variant="link">
                                    <i className="material-icons">more_horiz</i>
                                    </Dropdown.Toggle>
                                    <Dropdown.Menu className="dropdown-menu dropdown-menu-sm dropdown-menu-right" alignRight>
                                    <a className="dropdown-item" href="#">View</a>
                                    <a className="dropdown-item" href="#">Edit</a>
                                    <a className="dropdown-item" href="#">Delete</a>
                                    </Dropdown.Menu>
                                </Dropdown>
                                </td>
                            </tr>
                            </tbody>
                        </table>
                        </div>
                    </div>
                    </div>
                    <div className="col-12 col-md-6 col-lg-6 col-xl-4 mb-4">
                    <div className="card border-0 shadow-sm overflow-hidden h-100">
                        <div className="card-header border-0 bg-none">
                        <div className="row">
                            <div className="col">
                            <p className="fs15">ToDo Task<br /><small className="text-template-primary-light">32 Tasks</small></p>
                            </div>
                            <div className="col-auto align-self-center">
                            <button className="btn btn-sm btn-success" type="button">
                                Done
                            </button>
                            <button className="btn btn-sm btn-outline-template ml-2">
                                + Add
                            </button>
                            </div>
                        </div>
                        </div>
                        <div className="card-body scroll-y h-400 p-0">
                        <div className="list-group roudned-0">
                            <div className="list-group-item status-border border-warning">
                            <div className="row">
                                <div className="col-auto align-self-center">
                                <div className="custom-control custom-checkbox">
                                    <input type="checkbox" className="custom-control-input" id="checktask11" defaultChecked />
                                    <label className="custom-control-label" htmlFor="checktask11" />
                                </div>
                                </div>
                                <div className="col pl-0">
                                <p className="mb-0">6:33 | Attend Meeting</p>
                                <p className="text-template-primary-light">Aquaguaard Manager meeting</p>
                                </div>
                                <div className="col-auto pr-0 align-self-center">
                                <button className="btn btn-sm text-danger"><i className="material-icons">delete</i></button>
                                </div>
                            </div>
                            </div>
                            <div className="list-group-item status-border border-danger">
                            <div className="row">
                                <div className="col-auto align-self-center">
                                <div className="custom-control custom-checkbox">
                                    <input type="checkbox" className="custom-control-input" id="checktask12" />
                                    <label className="custom-control-label" htmlFor="checktask12" />
                                </div>
                                </div>
                                <div className="col pl-0">
                                <p className="mb-0">7:33 | Drink Water</p>
                                <p className="text-template-primary-light">Aquaguaard Manager meeting</p>
                                </div>
                                <div className="col-auto pr-0 align-self-center">
                                <button className="btn btn-sm text-danger"><i className="material-icons">delete</i></button>
                                </div>
                            </div>
                            </div>
                            <div className="list-group-item status-border border-warning">
                            <div className="row">
                                <div className="col-auto align-self-center">
                                <div className="custom-control custom-checkbox">
                                    <input type="checkbox" className="custom-control-input" id="checktask13" defaultChecked />
                                    <label className="custom-control-label" htmlFor="checktask13" />
                                </div>
                                </div>
                                <div className="col pl-0">
                                <p className="mb-0">8:00 | work for timesheet</p>
                                <p className="text-template-primary-light">Aquaguaard Manager meeting</p>
                                </div>
                                <div className="col-auto pr-0 align-self-center">
                                <button className="btn btn-sm text-danger"><i className="material-icons">delete</i></button>
                                </div>
                            </div>
                            </div>
                            <div className="list-group-item status-border border-success">
                            <div className="row">
                                <div className="col-auto align-self-center">
                                <div className="custom-control custom-checkbox">
                                    <input type="checkbox" className="custom-control-input" id="checktask14" />
                                    <label className="custom-control-label" htmlFor="checktask14" />
                                </div>
                                </div>
                                <div className="col pl-0">
                                <p className="mb-0">10:45 | Reverse proccess Training</p>
                                <p className="text-template-primary-light">Aquaguaard Manager meeting</p>
                                </div>
                                <div className="col-auto pr-0 align-self-center">
                                <button className="btn btn-sm text-danger"><i className="material-icons">delete</i></button>
                                </div>
                            </div>
                            </div>
                            <div className="list-group-item status-border border-primary">
                            <div className="row">
                                <div className="col-auto align-self-center">
                                <div className="custom-control custom-checkbox">
                                    <input type="checkbox" className="custom-control-input" id="checktask15" />
                                    <label className="custom-control-label" htmlFor="checktask15" />
                                </div>
                                </div>
                                <div className="col pl-0">
                                <p className="mb-0">6:33 | Attend Meeting</p>
                                <p className="text-template-primary-light">Aquaguaard Manager meeting</p>
                                </div>
                                <div className="col-auto pr-0 align-self-center">
                                <button className="btn btn-sm text-danger"><i className="material-icons">delete</i></button>
                                </div>
                            </div>
                            </div>
                            <div className="list-group-item status-border border-primary">
                            <div className="row">
                                <div className="col-auto align-self-center">
                                <div className="custom-control custom-checkbox">
                                    <input type="checkbox" className="custom-control-input" id="checktask16" />
                                    <label className="custom-control-label" htmlFor="checktask16" />
                                </div>
                                </div>
                                <div className="col pl-0">
                                <p className="mb-0">6:33 | Attend Meeting</p>
                                <p className="text-template-primary-light">Aquaguaard Manager meeting</p>
                                </div>
                                <div className="col-auto pr-0 align-self-center">
                                <button className="btn btn-sm text-danger"><i className="material-icons">delete</i></button>
                                </div>
                            </div>
                            </div>
                            <div className="list-group-item status-border border-primary">
                            <div className="row">
                                <div className="col-auto align-self-center">
                                <div className="custom-control custom-checkbox">
                                    <input type="checkbox" className="custom-control-input" id="checktask17" />
                                    <label className="custom-control-label" htmlFor="checktask17" />
                                </div>
                                </div>
                                <div className="col pl-0">
                                <p className="mb-0">6:33 | Attend Meeting</p>
                                <p className="text-template-primary-light">Aquaguaard Manager meeting</p>
                                </div>
                                <div className="col-auto pr-0 align-self-center">
                                <button className="btn btn-sm text-danger"><i className="material-icons">delete</i></button>
                                </div>
                            </div>
                            </div>
                        </div>
                        </div>
                    </div>
                    </div>
                </div>
                <div className="row">
                <div className="col-12 col-md-6 col-lg-6 col-xl-4 ">
                <div className="card border-0 shadow-sm overflow-hidden mb-4">
                    <div className="card-header border-0 bg-none">
                    <div className="row">
                        <div className="col">
                        <p className="fs15">Activity Wall<br /><small className="text-template-primary-light">Most recent activities done</small></p>
                        </div>
                    </div>
                    </div>
                    <div className="card-body scroll-y h-400 p-0">
                    <div className="list-group timeline-list roudned-0">
                        <div className="list-group-item border-0">
                        <div className="row">
                            <div className="col-auto">
                            <div className="timeline-circle-wrap">
                                <div className="timeline-circle border-warning"><span className="bg-warning" /></div>
                            </div>
                            </div>
                            <div className="col pl-0">
                            <p className="mb-0">We become world most valauble</p>
                            <p className="small text-mute">Thank you for all of your support and well wishes</p>
                            </div>
                        </div>
                        </div>
                        <div className="list-group-item border-0">
                        <div className="row">
                            <div className="col-auto">
                            <div className="timeline-circle-wrap">
                                <div className="timeline-circle border-danger"><span className="bg-danger" /></div>
                            </div>
                            </div>
                            <div className="col pl-0">
                            <p className="mb-0">Team Closer</p>
                            <p className="small text-template-primary-light mb-1">Maxartkiller Team</p>
                            </div>
                            <div className="col pl-0 align-self-center text-right">
                            <figure className="avatar avatar-30"><img src={userimage1} alt="AdminuxPRO" /></figure>
                            <figure className="avatar avatar-30 ml-1"><img src={userimage2} alt="AdminuxPRO" /></figure>
                            <figure className="avatar avatar-30 ml-1"><img src={userimage3} alt="AdminuxPRO" /></figure>
                            </div>
                        </div>
                        </div>
                        <div className="list-group-item border-0">
                        <div className="row">
                            <div className="col-auto">
                            <div className="timeline-circle-wrap">
                                <div className="timeline-circle border-success"><span className="bg-success" /></div>
                            </div>
                            </div>
                            <div className="col pl-0">
                            <p className="mb-0">Final Amendments are done </p>
                            <p className="small text-template-primary-light">Creative visual hierarchy completed</p>
                            </div>
                        </div>
                        </div>
                        <div className="list-group-item border-0">
                        <div className="row">
                            <div className="col-auto">
                            <div className="timeline-circle-wrap">
                                <div className="timeline-circle border-primary"><span className="bg-primary" /></div>
                            </div>
                            </div>
                            <div className="col pl-0">
                            <p className="mb-0">Vision deployment done</p>
                            <p className="small text-template-primary-light">UX designer worked upon timline</p>
                            </div>
                        </div>
                        </div>
                        <div className="list-group-item border-0">
                        <div className="row">
                            <div className="col-auto">
                            <div className="timeline-circle-wrap">
                                <div className="timeline-circle border-primary"><span className="bg-primary" /></div>
                            </div>
                            </div>
                            <div className="col pl-0">
                            <p className="mb-0">Vision deployment done</p>
                            <p className="small text-template-primary-light">UX designer worked upon timline</p>
                            </div>
                        </div>
                        </div>
                        <div className="list-group-item border-0">
                        <div className="row">
                            <div className="col-auto">
                            <div className="timeline-circle-wrap">
                                <div className="timeline-circle border-primary"><span className="bg-primary" /></div>
                            </div>
                            </div>
                            <div className="col pl-0">
                            <p className="mb-0">Vision deployment done</p>
                            <p className="small text-template-primary-light">UX designer worked upon timline</p>
                            </div>
                        </div>
                        </div>
                        <div className="list-group-item border-0">
                        <div className="row">
                            <div className="col-auto">
                            <div className="timeline-circle-wrap">
                                <div className="timeline-circle border-info"><span className="bg-info" /></div>
                            </div>
                            </div>
                            <div className="col pl-0">
                            <p className="mb-0">Critical Bug resolved</p>
                            <p className="small text-template-primary-light">Cleared list for bugs and development.</p>
                            </div>
                        </div>
                        </div>
                    </div>
                    </div>
                </div>
                </div>
                <div className="col-12 col-md-6 col-lg-6 col-xl-4">
                <div className="card border-0 shadow-sm mb-4">
                    <div className="card-header border-0 bg-none">
                    <div className="row">
                        <div className="col align-self-center">
                        <p className="fs15">Sales by Country<br /><small className="text-template-primary-light">Locate best popular country</small></p>
                        </div>
                    </div>
                    </div>
                    <div className="card-body pt-2">
                     <Vectormap></Vectormap>
                    <hr />
                    <div className="row">
                        <div className="col text-right">
                        <div className="row mb-2">
                            <div className="col pl-0">
                            <p>Sydeny <small className="d-block"><span className="text-template-primary">2548</span> <span className="text-danger">20%</span></small></p>
                            </div>
                            <div className="col-auto">
                            <i className="material-icons text-template-primary fs15 vm">album</i>
                            </div>
                        </div>
                        <div className="row mb-2">
                            <div className="col pl-0">
                            <p>WestIndies <small className="d-block"><span className="text-template-primary">2548</span> <span className="text-success">15%</span></small></p>
                            </div>
                            <div className="col-auto">
                            <i className="material-icons text-template-primary fs15 vm">album</i>
                            </div>
                        </div>
                        <div className="row">
                            <div className="col pl-0">
                            <p>Newzealand <small className="d-block"><span className="text-template-primary">2548</span> <span className="text-success">18%</span></small></p>
                            </div>
                            <div className="col-auto">
                            <i className="material-icons text-template-primary fs15 vm">album</i>
                            </div>
                        </div>
                        </div>
                        <div className="col">
                        <div className="row mb-2">
                            <div className="col-auto">
                            <i className="material-icons text-template-primary fs15 vm">album</i>
                            </div>
                            <div className="col pl-0">
                            <p>Brazil <small className="d-block"><span className="text-template-primary">2548</span> <span className="text-danger">20%</span></small></p>
                            </div>
                        </div>
                        <div className="row mb-2">
                            <div className="col-auto">
                            <i className="material-icons text-template-primary fs15 vm">album</i>
                            </div>
                            <div className="col pl-0">
                            <p>India <small className="d-block"><span className="text-template-primary">2548</span> <span className="text-success">15%</span></small></p>
                            </div>
                        </div>
                        <div className="row">
                            <div className="col-auto">
                            <i className="material-icons text-template-primary fs15 vm">album</i>
                            </div>
                            <div className="col pl-0">
                            <p>England <small className="d-block"><span className="text-template-primary">2548</span> <span className="text-success">18%</span></small></p>
                            </div>
                        </div>
                        </div>
                    </div>
                    </div>
                </div>
                </div>
                <div className="col-12 col-md-6 col-lg-6 col-xl-4">
                <div className="card border-0 shadow-sm mb-4">
                    <div className="card-header border-0 bg-none">
                    <div className="row">
                        <div className="col align-self-center">
                        <p className="fs15">New Users<br /><small className="text-template-primary-light">Most recent new registrations</small></p>
                        </div>
                    </div>
                    </div>
                    <div className="card-body ">
                    <div className="row">
                        <div className="col-6 mb-4 text-center">
                        <figure className="avatar avatar-110 mb-3">
                            <button className="btn btn-sm text-danger user-like"><i className="material-icons md-18">favorite</i></button>
                            <img src={userimage2} alt="AdminuxPRO" />
                        </figure>
                        <p className="mb-1">Anjali Govind</p>
                        <p className="text-template-primary-light">Team Lead</p>
                        </div>
                        <div className="col-6 mb-4 text-center">
                        <figure className="avatar avatar-110 mb-3">
                            <button className="btn btn-sm text-danger user-like"><i className="material-icons md-18">favorite</i></button>
                            <img src={userimage3} alt="AdminuxPRO" />
                        </figure>
                        <p className="mb-1">Sonya Wilson</p>
                        <p className="text-template-primary-light">Marketing Lead</p>
                        </div>
                        <div className="col-6 text-center">
                        <figure className="avatar avatar-110 mb-3">
                            <button className="btn btn-sm text-danger user-like"><i className="material-icons md-18">favorite</i></button>
                            <img src={userimage4} alt="AdminuxPRO" />
                        </figure>
                        <p className="mb-1">Syam Prashad</p>
                        <p className="text-template-primary-light">Production Manager</p>
                        </div>
                        <div className="col-6 text-center">
                        <figure className="avatar avatar-110 mb-3">
                            <button className="btn btn-sm text-danger user-like"><i className="material-icons md-18">favorite</i></button>
                            <img src={userimage5} alt="AdminuxPRO" />
                        </figure>
                        <p className="mb-1">John Doe</p>
                        <p className="text-template-primary-light">CEO, Aquaagaurd</p>
                        </div>
                    </div>
                    </div>
                </div>
                </div>
            </div>
                {/* Main container ends */}
            </div>
        );
    }
}

export default Production;